<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <title>Index Page</title>
</head>
<body>
    <div class="row">
        <div class="col-sm-4">
            <div class="card m-4" style = "background-color: #90EE90;">
                <div class="card-body">
                    <b style = "font-size: 20px;">About Me</b>
                </div>
                <div class="card-body" style = "background-color: white;">
                    Name: Joshua Maurice Yaacoub</br>
                    Year and Section: BSIT 3C</br>
                    Subject: Elective 1 (Web Systems and Technologies 2)</br>
                    <?php
                        $offset = strtotime("+7 hours");
                        $date = date("m-d-Y H:i:s", $offset);
                        echo "Date and Time: $date </br>";
                    ?>
                </div>
            </div>
        </div>
    </div>
</body>
</html>