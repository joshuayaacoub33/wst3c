<?php
    //Include Facebook Configuration File
    include('fconfig.php');
    if($_SESSION['facebook_access_token'] == '') {
    header("location: index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Homepage</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <style>
        a{
            text-decoration: none;
            color: black;
            font-weight: bold;
        }
        a:hover{
            color: white;
            font-weight: bold;
        }
        button:hover{
            color: white;
            font-weight: bold;
        }
    </style>
</head>
<body>
        <div class="mx-auto" style = "max-width: 30rem; margin-top: 100px;">
            <div class="card-header bg-dark text-light">
                <?php 
                    $offset = strtotime("+7 hours");
                    $date = date("m-d-Y");
                    $time = date("H:i:s", $offset);
                    if(!empty($_SESSION['fb_user_pic'])){
                        echo "<center><img src = '$_SESSION[fb_user_pic]' height = '120px' width = '120px'
                        style = 'margin-top:10px; margin-bottom: 10px;'></center>";
                    }
                    if(isset($_SESSION['fb_user_id'])){
                        if(!empty($_SESSION['fb_user_name'])){
                            echo "Welcome, " . $_SESSION['fb_user_name'];
                        }
                    }
                    for($i = 0; $i < 22; $i++){
                        echo "&ensp;";
                    }
                    echo "$date</br>";
                    echo "Year and Section: BSIT-3C";
                    for($i = 0; $i < 26; $i++){
                        echo "&ensp;";
                    }
                    echo "$time";
                ?>
            </div>
            <div class ="card-body bg-light border-primary">
                <center>
                    <h2>Welcome FB/GOOGLE</h2>
                    <button class = "btn btn-warning"><a href="logout.php">LOGOUT</a></button>
                </center>
            </div>
        </div>
</body>
</html>