<?php

//logout.php

include('gconfig.php');
require_once 'fconfig.php';

//Reset OAuth access token
$google_client->revokeToken();

//Destroy entire session data.
session_destroy();

//redirect page to index.php
header('location:index.php');

unset($_SESSION['facebook_access_token']);


unset($_SESSION['fb_user_id']);
unset($_SESSION['fb_user_name']);
unset($_SESSION['fb_user_email']);
unset($_SESSION['fb_user_pic']);


header("location:index.php");

?>