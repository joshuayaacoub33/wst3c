<?php

    use Facebook\Exceptions\FacebookResponseException;
    use Facebook\Exceptions\FacebookSDKException;
    require_once(__DIR__.'/Facebook/autoload.php');

    define('APP_ID','3182039935401620');
    define('APP_SECRET','36ddeb5df7a1b0b091f551ee82ac287f');
    define('API_VERSION', 'v2.5');
    define('FB_BASE_URL','http://localhost/projects/web_projects/Activity2_Yaacoub/dashboard_fb.php');
    if(!session_id()){
        session_start();
    }

    // Call Facebook API
    $fb = new Facebook\Facebook([
        'app_id' => APP_ID,
        'app_secret' => APP_SECRET,
        'default_graph_version' => API_VERSION,
    ]);
    
    
    // Get redirect login helper
    $fb_helper = $fb->getRedirectLoginHelper();
    
    
    // Try to get access token
    try {
        if(isset($_SESSION['facebook_access_token']))
            {$accessToken = $_SESSION['facebook_access_token'];}
        else
            {$accessToken = $fb_helper->getAccessToken();}
    } catch(FacebookResponseException $e) {
            echo 'Facebook API Error: ' . $e->getMessage();
            exit;
    } catch(FacebookSDKException $e) {
        echo 'Facebook SDK Error: ' . $e->getMessage();
            exit;
    }

    $permissions = ['email']; //optional

    if (isset($accessToken))
    {
        if (!isset($_SESSION['facebook_access_token'])) 
        {
            //get short-lived access token
            $_SESSION['facebook_access_token'] = (string) $accessToken;
            
            //OAuth 2.0 client handler
            $oAuth2Client = $fb->getOAuth2Client();
            
            //Exchanges a short-lived access token for a long-lived one
            $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
            $_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
            
            //setting default access token to be used in script
            $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
        } 
        else 
        {
            $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
        }
        
        
        //redirect the user to the index page if it has $_GET['code']
        if (isset($_GET['code'])) 
        {
            header("location: dashboard_fb.php");
        }
        
        
        try {
            $fb_response = $fb->get('/me?fields=name,first_name,last_name,email');		
            $fb_user = $fb_response->getGraphUser();
            $fb_response_picture = $fb->get('/me/picture?redirect=false&height=200');
            $picture = $fb_response_picture->getGraphUser();
            
            $_SESSION['fb_user_id'] = $fb_user->getField('id');
            $_SESSION['fb_user_name'] = $fb_user->getField('name');
            $_SESSION['fb_user_email'] = $fb_user->getField('email');
            $_SESSION['fb_user_pic'] = $picture['url'];
            
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Facebook API Error: ' . $e->getMessage();
            session_destroy();
            // redirecting user back to app login page
            header("location: index.php");
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK Error: ' . $e->getMessage();
            exit;
        }
    } 
    else 
    {	
        // replace your website URL same as added in the developers.Facebook.com/apps e.g. if you used http instead of https and you used
        $fb_login_url = $fb_helper->getLoginUrl(FB_BASE_URL, $permissions);
    }
?>