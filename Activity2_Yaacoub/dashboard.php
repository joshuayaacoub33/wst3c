<?php

//Include Google Configuration File
include('gconfig.php');
if($_SESSION['access_token'] == '') {
  header("location: index.php");
}

//This $_GET["code"] variable value received after user has login into their Google Account redirct to PHP script then this variable value has been received
if(isset($_GET["code"]))
{
 //It will Attempt to exchange a code for an valid authentication token.
 $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

 //This condition will check there is any error occur during geting authentication token. If there is no any error occur then it will execute if block of code/
 if(!isset($token['error']))
 {
  //Set the access token used for requests
  $google_client->setAccessToken($token['access_token']);

  //Store "access_token" value in $_SESSION variable for future use.
  $_SESSION['access_token'] = $token['access_token'];

  //Create Object of Google Service OAuth 2 class
  $google_service = new Google_Service_Oauth2($google_client);

  //Get user profile data from google
  $data = $google_service->userinfo->get();

  //Below you can find Get profile data and store into $_SESSION variable
  if(!empty($data['given_name']))
  {
   $_SESSION['user_first_name'] = $data['given_name'];
  }

  if(!empty($data['family_name']))
  {
   $_SESSION['user_last_name'] = $data['family_name'];
  }

  if(!empty($data['email']))
  {
   $_SESSION['user_email_address'] = $data['email'];
  }

  if(!empty($data['gender']))
  {
   $_SESSION['user_gender'] = $data['gender'];
  }

  if(!empty($data['picture']))
  {
   $_SESSION['user_image'] = $data['picture'];
  }
 }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Homepage</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <style>
        a{
            text-decoration: none;
            color: black;
            font-weight: bold;
        }
        a:hover{
            color: white;
            font-weight: bold;
        }
        button:hover{
            color: white;
            font-weight: bold;
        }
    </style>
</head>
<body>
        <div class="mx-auto" style = "max-width: 30rem; margin-top: 100px;">
            <div class="card-header bg-dark text-light">
                <?php 
                    $offset = strtotime("+7 hours");
                    $date = date("m-d-Y");
                    $time = date("H:i:s", $offset);
                    if(!empty($_SESSION['user_image'])){
                        echo "<center><img src = '$_SESSION[user_image]' height = '120px' width = '120px'
                        style = 'margin-top:10px; margin-bottom: -5px;'></center></br>";       
                    }
                    if(!empty($_SESSION['user_first_name']) && !empty($_SESSION['user_last_name'])){
                        echo "Welcome, " . $_SESSION['user_first_name'].' '.$_SESSION['user_last_name']; 
                    }
                    for($i = 0; $i < 15; $i++){
                        echo "&ensp;";
                    }
                    echo "$date</br>";
                    echo "Year and Section: BSIT-3C";
                    for($i = 0; $i < 26; $i++){
                        echo "&ensp;";
                    }
                    echo "$time";
                ?>
            </div>
            <div class ="card-body bg-light border-primary">
                <center>
                    <h2>Welcome FB/GOOGLE</h2>
                    <button class = "btn btn-warning"><a href="logout.php">LOGOUT</a></button>
                </center>
            </div>
        </div>
</body>
</html>