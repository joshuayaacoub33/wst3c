<?php

namespace App\Http\Controllers;

use App\Models\Certificate;
use App\Models\Training;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class MainController extends Controller
{
    function dashboard(){

        $data = array(
            'list'=>DB::table('training')->get(),
        );
        

        return view('admin.dashboard', $data);
    }

    function addtraining(Request $request){

        $validator = Validator::make($request->all(), [
            'training' => 'required',
            'image' => 'required|mimes:png|image',
            'date' => 'required',
        ]);
        
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else{
            if($request->hasFile('image')){
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $extension;
                $file->move('assets/image/logo/', $filename);   

                $query = DB::table('training')->insert([
                    'training'=> $request-> input('training'),
                    'image' => $filename,
                    'logo' => $request->get('template'),
                    'date'=> $request-> input('date'),
                    'status' => 'ACTIVE'
                ]);
        
                if($query){
                    return back()->with('success', 'Seminar set successful!');
                } else
                    return back()->with('fail', 'Seminar set unsuccessful!');
            }
        }
    }

    function deletetraining($id){
        $certificate = Certificate::where('training_id', $id);
        $query = DB::delete('DELETE FROM training WHERE training_id = ?', [$id]);
       
        if($query){
            $certificate->delete();
            return redirect()->back()->with('success', 'Data deleted');
        } else
            return redirect()->back()->with('fail', 'Data not deleted');

    }

    function edittraining($id){
        $edit = DB::select('select * from training where training_id = ?', [$id]);
        return view('admin.edit', ['edit' => $edit]);
    }

    function viewtraining($id){
        $id = request()->segment(2);
        $fetch = DB::select('select * from certificates where training_id = ?', [$id]);
        $select = DB::select('select * from training where training_id = ?', [$id]);
        $data = array(
            'id'=>DB::table('training')->where('training_id', $id)->pluck('training'),
            'list'=>DB::table('training')->where('training_id', $id)->first()
        );
        return view('admin.generate', $data)->with('fetch', $fetch)->with('train', request()->segment(2))->with('select', $select);
    }

    function updatetraining(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'training' => 'required',
            'date' => 'required',
        ]);
        $training = $request->get('training');
        $date = $request->get('date');
        $template = $request->get('template');
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else{
            if($request->hasFile('image')){
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $extension;
                $file->move('assets/image/logo/', $filename);   
    
                $query = DB::table('training')->where('training_id', $request->segment(2))->update([
                    'training'=> $request-> input('training'),
                    'image' => $filename,
                    'logo' => $template,
                    'date'=> $request-> input('date'), 
                ]);
        
                if($query){
                    return back()->with('success', 'Seminar set successful!');
                } else
                    return back()->with('fail', 'Seminar set unsuccessful!');
            }
            DB::update('update training set training = "'.$training.'", logo = "'.$template.'", date = "'.$date.'" where training_id = ?' ,[$id]);
            return back()->with('success', 'Seminar set successful!');
        } 
    }
    public function active($id)
    {
        DB::table('training')->where('training_id', $id)->update(['status' => 'ACTIVE']);
        return redirect()->back()->with('update', 'Status set to active!');
    }

    public function inactive($id)
    {
        DB::table('training')->where('training_id', $id)->update(['status' => 'INACTIVE']);
        return redirect()->back()->with('update', 'Status set to inactive!');
    }

    function searchCert(Request $request){
        $validator = Validator::make($request->all(), [
            'code' => 'min:32|max:32|required',
        ]);
        $id = $request-> input('code');
        if($validator->fails())
        {
            return redirect()->back()->with('invalid_code', 'Please enter a valid certificate id');
        }
        else
        {
            $query = DB::select('select * from certificates INNER JOIN training ON certificates.training_id = training.training_id WHERE certificates.certificate_id = "'.$id.'";');
            return view('admin.search')->with('fetch', $query);
        }
    }

    function searchUserCert(Request $request){
        $validator = Validator::make($request->all(), [
            'code' => 'min:32|max:32|required',
        ]);
        $id = $request-> input('code');
        if($validator->fails())
        {
            return redirect()->back()->with('invalid_code', 'Please enter a valid certificate id');
        }
        else
        {
            $query = DB::select('select * from certificates INNER JOIN training ON certificates.training_id = training.training_id WHERE certificates.certificate_id = "'.$id.'";');
            return view("search")->with('fetch', $query);
        }
    }

    function userView(){
        return view('index');
    }
}
