<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        $populate = DB::select('select * from users');
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.admin.manage_user')->with('fetch', $fetch)->with('posts', $populate);
    }

    public function create(Request $request)
    {
        $user = new User();
        $validator = Validator::make($request->all(), [
            'username' => 'required|unique:users',
            'email' => 'required|unique:users',
            'password' => 'required|min:6'
        ]);

        $username = $request->get('username');
        $email = $request->get('email');
        $password =  Hash::make($request->get('password'));

        if($validator->fails()){
            return response()->json(['error' => $validator->errors()->all()]);
        }
        else{
            $user->username = $username;
            $user->email = $email;
            $user->password = $password;
            $user->save();
            return response()->json(['successMsg' => 'Success']); 
        }
    }

    public function delete(Request $request)
    {
        $id= $request->id;
        $user = User::find($id);
        $user->delete();
        return redirect()->back();
    }

    public function edit($id)
    {
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        $post = User::find($id);
        return view('pages.admin.edit_user')->with('fetch', $fetch)->with('post', $post);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|unique:users',
            'email' => 'required|unique:users',
            'password' => 'required|min:6'
        ]);

        $username = $request->get('username');
        $email = $request->get('email');
        $password =  Hash::make($request->get('password'));

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else{
            DB::update('update users set username = ?, email = ?, password = ? where id = ?', [$username, $email, $password, $id]);
            return redirect()->back()->with('success', 'User Updated Successfully!');
        }
    }
}
