<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where('postType', 'announcement')->orderBy('created_at', 'desc')->paginate(6); // Post::all();
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.admin.announcements')->with('posts', $posts)->with('fetch', $fetch);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:posts|max:255',
            'subtitle' => 'required',
            'description' => 'required',
            'image' => 'required|unique:posts|image|mimes:png,jpeg,gif,jpg',
        ]);
        
        $title = $request->get('title');
        $subtitle = $request->get('subtitle');
        $description = $request->get('description');

        if($validator->fails()){
            return redirect('/admin/announcements')->withErrors($validator)->withInput();
        }
        else{
            if($request->hasFile('image')){
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $extension;
                $file->move('assets/images/database_image/', $filename);
                $create = date('Y-m-d H:i:s');
                $update = date('Y-m-d H:i:s');
                DB::insert('insert into posts (title, subtitle, description, image, postType, created_at, updated_at) values (?, ?, ?, ?, ?, ?, ?)', [$title, $subtitle, $description, $filename, 'announcement', $create, $update]);
            }
            return redirect('/admin/announcements')->with('success', 'Post Created Successfully!'); 
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.admin.show')->with('post', $post)->with('fetch', $fetch);;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $select = Post::find($id);
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.admin.edit_news')->with('post', $select)->with('fetch', $fetch);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'subtitle' => 'required',
            'desc' => 'required',
            'image' => 'image|mimes:png,jpg,jpeg'
        ]);
        
        $title = $request->get('title');
        $subtitle = $request->get('subtitle');
        $description = $request->get('desc');

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else{
            if($request->hasFile('image')){
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $extension;
                $file->move('assets/images/database_image/', $filename);
                $update = date('Y-m-d H:i:s');
                DB::update('update posts set title = "'.$title.'", subtitle = "'.$subtitle.'", 
                description = "'.$description.'", image = "'.$filename.'", updated_at = "'.$update.'" where id = ?', [$id]);
            }
            else{
                $update = date('Y-m-d H:i:s');
                DB::update('update posts set title = "'.$title.'", subtitle = "'.$subtitle.'", 
                description = "'.$description.'", updated_at = "'.$update.'" where id = ?', [$id]);
            }
            return redirect()->back()->with('update', 'Post Updated Successfully!'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id= $request->id;
        $post = Post::find($id);
        $post->delete();
        return redirect()->back();
    }
}
