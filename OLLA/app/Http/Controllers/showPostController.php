<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class showPostController extends Controller
{

    public function showAnnouncement()
    {
        $posts = Post::where('postType', 'announcement')->orderBy('created_at', 'desc')->paginate(6);
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.user.announcements')->with('posts', $posts)->with('fetch', $fetch);
    }
    public function showAnnouncementPage($id)
    {
        $post = Post::find($id);
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.user.show')->with('post', $post)->with('fetch', $fetch);
    }

    public function showEvent()
    {
        $posts = Post::where('postType', 'event')->orderBy('created_at', 'desc')->paginate(6);
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.user.events')->with('posts', $posts)->with('fetch', $fetch);
    }

    public function showEventPage($id)
    {
        $post = Post::find($id);
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.user.show')->with('post', $post)->with('fetch', $fetch);
    }

    public function showAdminEvent()
    {
        $posts = Post::where('postType','event')->orderBy('created_at', 'desc')->paginate(6);
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.admin.events')->with('posts', $posts)->with('fetch', $fetch);
    }

    public function showAdminEventPage($id)
    {
        $post = Post::find($id);
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.admin.show')->with('post', $post)->with('fetch', $fetch);
    }

    public function createEvent(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:posts|max:255',
            'subtitle' => 'required',
            'description' => 'required',
            'image' => 'required|unique:posts|image|mimes:png,jpeg,gif,jpg',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);
        
        $title = $request->get('title');
        $subtitle = $request->get('subtitle');
        $description = $request->get('description');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');

        if($validator->fails()){
            return redirect('/admin/events')->withErrors($validator)->withInput();
        }
        else{
            if($request->hasFile('image')){
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $extension;
                $file->move('assets/images/database_image/', $filename);
                $create = date('Y-m-d H:i:s');
                $update = date('Y-m-d H:i:s');
                DB::insert('insert into posts (title, subtitle, description, image, postType, start_date, end_date, created_at, updated_at) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', [$title, $subtitle, $description, $filename, 'event', $start_date, $end_date, $create, $update]);
            }
            return redirect('/admin/events')->with('success', 'Post Created Successfully!'); 
        }
    }

    public function delete(Request $request){
        $id= $request->id;
        $post = Post::find($id);
        $post->delete();
        return redirect()->back();
    }

    public function edit($id)
    {
        $select = Post::find($id);
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.admin.edit_event')->with('post', $select)->with('fetch', $fetch);
    }

    public function show($id){
        $select = DB::select('select * from posts where id = ?', [$id]);
        return redirect('/admin/events')->with('post', $select);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'subtitle' => 'required',
            'desc' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'image' => 'image|mimes:png,jpg,jpeg'
        ]);
        
        $title = $request->get('title');
        $subtitle = $request->get('subtitle');
        $description = $request->get('desc');
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else{
            if($request->hasFile('image')){
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $extension;
                $file->move('assets/images/database_image/', $filename);
                $update = date('Y-m-d H:i:s');
                DB::update('update posts set title = "'.$title.'", subtitle = "'.$subtitle.'", 
                description = "'.$description.'", image = "'.$filename.'",
                start_date = "'.$start_date.'", end_date = "'.$end_date.'" ,updated_at = "'.$update.'" where id = ?', [$id]);
            }
            else{
                $update = date('Y-m-d H:i:s');
                DB::update('update posts set title = "'.$title.'", subtitle = "'.$subtitle.'", 
                description = "'.$description.'", start_date = "'.$start_date.'", end_date = "'.$end_date.'" ,updated_at = "'.$update.'" where id = ?', [$id]);
            }
            return redirect()->back()->with('update', 'Post Updated Successfully!'); 
        }
    }
}
