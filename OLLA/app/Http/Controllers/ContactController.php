<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function index()
    {
        return view('pages.user.home');
    }

    public function sendEmail(Request $request)
    {
        $request->validate([
            'Name' => 'required',
            'Email' => 'required|email',
            'Subject' => 'required',
            'Message' => 'required|min:8|max:255',
        ]);
        $mail_data = [
            'fromEmail' => $request->Email,
            'fromName' => $request->Name,
            'subject' => $request->Subject,
            'body' => $request->Message
        ];
        if($this->isOnline()){
            Mail::to('joshuayaacoub33@gmail.com')->send(new ContactMail($mail_data));
            return redirect()->back()->with('success', 'Email sent');
        }
        else{
            return redirect()->back()->with('internet_error', 'Unable to send message, please check your internet connection');
        }
    }

    public function isOnline($site = "https://youtube.com")
    {
        if(@fopen($site,"r")){
            return true;
        }else{
            return false;
        }
    }
}
