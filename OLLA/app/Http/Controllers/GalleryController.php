<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class GalleryController extends Controller
{
    public function index()
    {
        $gallery = Gallery::paginate(6);
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.user.gallery')->with('fetch', $fetch)->with('gallery', $gallery);
    }
    public function admin_index()
    {   
        $gallery = Gallery::paginate(6);
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.admin.gallery')->with('fetch', $fetch)->with('gallery', $gallery);
    }
    
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'gallery_description' => 'required|max:255',
            'gallery_image' => 'required|mimes:png,jpg,jpeg',
        ]);
        
        $description = $request->get('gallery_description');
        $gallery = new Gallery();

        if($validator->fails()){
            return response()->json(['error' => $validator->errors()->all()]);
        }
        else{
            if($request->hasFile('gallery_image')){
                $file = $request->file('gallery_image');
                $extension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $extension;
                $file->move('assets/images/gallery/', $filename);
                $gallery->description = $description;
                $gallery->image = $filename;
                $gallery->save();
            }
            return response()->json(['successMsg' => 'Success']); 
        }
    }

    public function edit($id)
    {   
        $gallery = Gallery::find($id);
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.admin.edit_gallery')->with('fetch', $fetch)->with('post', $gallery);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'gallery_description' => 'required|max:255',
            'staff_image' => 'mimes:png,jpg,jpeg',
        ]);
        

        $gallery = Gallery::find($id);
        $description = $request->input('gallery_description');

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else{
            if($request->hasFile('image')){
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $extension;
                $file->move('assets/images/gallery/', $filename);
                $gallery->description = $description;
                $gallery->image = $filename;
                $gallery->save();
            }
            else
            {
                $gallery->description = $description;
                $gallery->save();
            }
            return redirect()->back()->with('update', 'Gallery Updated Successfully!'); 
        }
    }

    public function delete(Request $request)
    {
        $id= $request->id;
        $gallery = Gallery::find($id);
        $gallery->delete();
        return redirect()->back();
    }
}
