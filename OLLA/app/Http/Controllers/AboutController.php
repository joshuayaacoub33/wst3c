<?php

namespace App\Http\Controllers;

use App\Models\About;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AboutController extends Controller
{
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description' => 'required',
        ]);

        $about = new About();
        $description = $request->input('description');
        
        if($validator->fails())
        {
            return response()->json(['error' => $validator->errors()->all()]);
        }
        else
        {
            
            $about->description = $description;
            $about->save();
            return response()->json(['success' => 'Success']); 
        }
    }
    
    public function delete(Request $request)
    {
        $id= $request->id;
        $post = About::find($id);
        $post->delete();
        return redirect()->back();
    }

    public function edit($id)
    {
        $fetch = About::find($id);
        $post= DB::select('select * from users where id = ?', [1]);
        return view('pages.admin.edit_about')->with('post', $fetch)->with('fetch', $post);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'description' => 'required',
        ]);

        $description = $request->get('description');
        
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else
        {
            $about = About::where('id', $id)->update(['description' => $description]);
            return redirect()->back()->with('update', 'Success');
        }
    }
}
