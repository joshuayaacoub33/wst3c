<?php

namespace App\Http\Controllers;

use App\Models\About;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LogoController extends Controller
{
    public function index()
    {
        $select = Post::where('postType', 'facilities')->paginate(6);
        $latest = DB::select('select * from posts where postType =  ? order by created_at desc limit 3', ['announcement']);
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.admin.home')->with('fetch', $fetch)->with('post', $select)->with('latest', $latest);
    }

    public function home()
    {
        $select = Post::where('postType', 'facilities')->paginate(6);
        $latest = DB::select('select * from posts where postType =  ? order by created_at desc limit 3', ['announcement']);
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.user.index')->with('fetch', $fetch)->with('post', $select)->with('latest', $latest);;
    }
    public function announce()
    {
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.user.announcements')->with('fetch', $fetch);
    }

    public function event()
    {
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.user.events')->with('fetch', $fetch);
    }

    public function about()
    {
        $about = About::all();
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.user.about')->with('fetch', $fetch)->with('about', $about);
    }
    
    public function admin_announce()
    {
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.admin.announcements')->with('fetch', $fetch);
    }

    public function admin_event()
    {
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.admin.events')->with('fetch', $fetch);
    }

    public function admin_about()
    {
        $about = About::all();
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.admin.about')->with('fetch', $fetch)->with('about', $about);
    }

    public function admin_show()
    {
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.admin.show')->with('fetch', $fetch);
    }


    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'logo' => 'required|image|mimes:png,jpg,jpeg',
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors()->all()
            ]);
        }
        else{
            if($request->hasFile('logo')){
                $file = $request->file('logo');
                $extension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $extension;
                $file->move('assets/icons/database_icon/', $filename);
                // User::find(auth()->user()->id)->update(['company_logo'=> $filename]);
                DB::update('update users set company_logo = "'.$filename.'" where id = ?', [1]);
                return response()->json(['success' => 'Logo updated successfully.']);  
            }
        }
    }
}
