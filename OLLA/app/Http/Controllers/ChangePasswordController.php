<?php
   
namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
  
class ChangePasswordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
   
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('pages.admin.home');
    } 
   
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => ['required', 'min:6', 'alphaNum'],
            'new_password' => ['required', 'min:6', 'alphaNum'],
            'new_confirm_password' => ['required', 'same:new_password' , 'min:6'],
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors()->all()
            ]);
        }
        else{
            User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_confirm_password)]);
            return response()->json(['success' => 'Password updated successfully.']);  
        }
    }
}