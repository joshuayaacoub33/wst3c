<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class FacilitiesController extends Controller
{
    public function delete(Request $request){
        $id= $request->id;
        $post = Post::find($id);
        $post->delete();
        return redirect()->back();
    }

    public function edit($id)
    {
        $select = Post::find($id);
        $fetch = DB::select('select * from users where id = ?', [1]);
        return view('pages.admin.edit_facilities')->with('post', $select)->with('fetch', $fetch);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'image' => 'required|mimes:png,jpg,jpeg',
        ]);
        
        $title = $request->input('title');

        if($validator->fails()){
            return response()->json(['error' => $validator->errors()->all()]);
        }
        else{
            if($request->hasFile('image')){
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $extension;
                $file->move('assets/images/facilities/', $filename);
                $create = date('Y-m-d H:i:s');
                $update = date('Y-m-d H:i:s');
                DB::insert('insert into posts (title, image, postType, created_at, updated_at) values (?, ?, ?, ?, ?)', [$title, $filename, 'facilities', $create, $update]);
            }
            return response()->json(['successMsg' => 'Success']); 
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'image' => 'image|mimes:png,jpg,jpeg',
        ]);
        
        $title = $request->input('title');

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else{
            if($request->hasFile('image')){
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $extension;
                $file->move('assets/images/facilities/', $filename);
                $update = date('Y-m-d H:i:s');
                DB::update('update posts set title = ?, image = ?, updated_at = ? where id = ?', [$title, $filename, $update, $id]);
            }
            else{
                $update = date('Y-m-d H:i:s');
                DB::update('update posts set title = ?, updated_at = ? where id = ?', [$title, $update, $id]);
            }
            return redirect()->back()->with('update', 'Post Updated Successfully!'); 
        }
    }
}