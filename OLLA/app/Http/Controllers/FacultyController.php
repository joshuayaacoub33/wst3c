<?php

namespace App\Http\Controllers;

use App\Models\Faculty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class FacultyController extends Controller
{
    public function index()
    {   
        $faculty = Faculty::paginate(6);
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.user.faculty')->with('fetch', $fetch)->with('faculty', $faculty);
    }

    public function admin_index()
    {   
        $faculty = Faculty::paginate(6);
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.admin.faculty')->with('fetch', $fetch)->with('faculty', $faculty);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'position' => 'required|max:255',
            'staff_image' => 'required|mimes:png,jpg,jpeg',
        ]);
        

        $faculty = new Faculty();
        $name = $request->input('name');
        $position = $request->input('position');

        if($validator->fails()){
            return response()->json(['error' => $validator->errors()->all()]);
        }
        else{
            if($request->hasFile('staff_image')){
                $file = $request->file('staff_image');
                $extension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $extension;
                $file->move('assets/images/faculty/', $filename);
                $faculty->name = $name;
                $faculty->position = $position;
                $faculty->image = $filename;
                $faculty->save();
            }
            return response()->json(['successMsg' => 'Success']); 
        }
    }

    public function edit($id)
    {   
        $gallery = Faculty::find($id);
        $fetch = DB::select('select company_logo from users where id = ?', [1]);
        return view('pages.admin.edit_faculty')->with('fetch', $fetch)->with('post', $gallery);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'position' => 'required|max:255',
            'staff_image' => 'mimes:png,jpg,jpeg',
        ]);
        

        $faculty = Faculty::find($id);
        $name = $request->input('name');
        $position = $request->input('position');

        if($validator->fails()){
            return response()->json(['error' => $validator->errors()->all()]);
        }
        else{
            if($request->hasFile('staff_image')){
                $file = $request->file('staff_image');
                $extension = $file->getClientOriginalExtension();
                $filename = time() . '.' . $extension;
                $file->move('assets/images/faculty/', $filename);
                $faculty->name = $name;
                $faculty->position = $position;
                $faculty->image = $filename;
                $faculty->save();
            }
            else{
                $faculty->name = $name;
                $faculty->position = $position;
                $faculty->save();
            }
            return redirect()->back()->with('update', 'Faculty Updated Successfully!'); 
        }
    }
    public function delete(Request $request)
    {
        $id= $request->id;
        $faculty = Faculty::find($id);
        $faculty->delete();
        return redirect()->back();
    }
}
