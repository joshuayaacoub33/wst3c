<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    function index()
    {
        return view('pages.admin.admin');
    }

    function checkLogin(Request $request)
    {
        $this->validate($request, [
            'username'=>'required',
            'password'=>'required|alphaNum|min:6',
        ]);

        $user_data = array(
            'username' =>  $request->get('username'),
            'password'  =>  $request->get('password'),
        );

        if(Auth::attempt($user_data)){
            return redirect('admin/home');
        }
        else{
            return back()->with('error', 'Wrong Login Details!');
        }
    }

    function successLogin()
    {
        return view('pages.admin.home');
    }

    function logout()
    {
        Auth::logout();
        return redirect('admin');
    }
}
