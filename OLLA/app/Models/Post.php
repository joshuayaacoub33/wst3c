<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    
    protected $table = 'posts';
    public $primary_key = 'id';
    public $timestamps = true;

    protected $fillable = [
        'title',
        'subtitle',
        'description',
        'image',
        'postType',
        'start_date',
        'end_date',
        'created_at',
        'updated_at'
    ];
}
