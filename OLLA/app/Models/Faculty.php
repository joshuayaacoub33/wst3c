<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    use HasFactory;
    protected $table = 'faculties';
    public $primary_key = 'id';
    public $timestamps = true;

    protected $fillable = [
        'image',
        'name',
        'position',
        'created_at',
        'updated_at'
    ];
}
