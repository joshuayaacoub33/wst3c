<?php

use App\Http\Controllers\AboutController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\FacilitiesController;
use App\Http\Controllers\FacultyController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\LogoController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\showPostController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Routes for UserController
Route::get('/admin/manage-user', [UserController::class, 'index']);
Route::post('/admin/manage-user/create', [UserController::class, 'create']);
Route::delete('/admin/manage-user/delete/{id}', [UserController::class, 'delete'])->name('delete.user');
Route::get('/admin/manage-user/edit/{id}', [UserController::class, 'edit']);
Route::post('/admin/manage-user/update/{id}', [UserController::class, 'update'])->name('update.user');

// Routes for LoginController
Route::get('admin', [LoginController::class, 'index'])->name('admin.home');
Route::post('admin/checkLogin', [LoginController::class, 'checkLogin']);
Route::get('admin/home', [LoginController::class, 'successLogin']);
Route::get('admin/logout', [LoginController::class, 'logout']);

// Routes for ChangePasswordController
Route::get('admin/home', [ChangePasswordController::class, 'index']);
Route::post('admin/home', [ChangePasswordController::class, 'store'])->name('change.password');

// Route for ContactController
Route::get('/', [ContactController::class, 'index']);
Route::post('/', [ContactController::class, 'sendEmail'])->name('send.email');

// Route for PostAnnouncement
Route::resource('/admin/announcements', PostsController::class);
Route::post('/admin/announcements', [PostsController::class, 'create'])->name('post.announcement.create');
Route::get('/admin/announcements/post/{id}', [PostsController::class, 'show']);
Route::get('/admin/announcements/edit/{id}', [PostsController::class, 'edit']);
Route::post('/admin/announcements/update/{id}', [PostsController::class, 'update'])->name('post.announcement.update');
Route::delete('/admin/announcements/delete/{id}', [PostsController::class, 'destroy'])->name('post.announcement.delete');


Route::post('/admin/events', [showPostController::class, 'createEvent'])->name('post.events.create');
Route::delete('/admin/events/delete/{id}', [showPostController::class, 'delete'])->name('post.events.delete');
Route::get('/admin/events/post/{id}', [showPostController::class, 'show']);
Route::get('/admin/events/edit/{id}', [showPostController::class, 'edit']);
Route::post('/admin/events/update/{id}', [showPostController::class, 'update'])->name('post.events.update');

Route::get('/announcements', [showPostController::class, 'showAnnouncement']);
Route::get('/announcements/post/{id}', [showPostController::class, 'showAnnouncementPage']);
Route::get('/events', [showPostController::class, 'showEvent']);
Route::get('/events/post/{id}', [showPostController::class, 'showEventPage']);

Route::get('/admin/events', [showPostController::class, 'showAdminEvent']);
Route::get('/admin/events/post/{id}', [showPostController::class, 'showAdminEventPage']);

// Route for LogoController
Route::get('admin/home', [LogoController::class, 'index']);
Route::get('/', [LogoController::class, 'home']);
Route::get('/about', [LogoController::class, 'about']);
Route::get('/admin/about', [LogoController::class, 'admin_about']);
Route::post('admin/home/update', [LogoController::class, 'update'])->name('update.logo');

// Route for FacilitiesController   
Route::post('/admin/home/create', [FacilitiesController::class, 'create'])->name('add.facilities');
Route::delete('/admin/home/facilities/delete/{id}', [FacilitiesController::class, 'delete'])->name('delete.facilities');
Route::get('/admin/home/facilities/edit/{id}', [FacilitiesController::class, 'edit']);
Route::post('/admin/home/facilities/update/{id}', [FacilitiesController::class, 'update'])->name('update.facilities');

// Route for AboutController
Route::post('/admin/about/create', [AboutController::class, 'create'])->name('add.about');
Route::delete('/admin/about/delete/{id}', [AboutController::class, 'delete'])->name('delete.about');
Route::get('/admin/about/edit/{id}', [AboutController::class, 'edit']);
Route::post('/admin/about/update/{id}', [AboutController::class, 'update'])->name('update.about');

// Route for GalleryController
Route::get('/gallery', [GalleryController::class, 'index']);
Route::get('/admin/gallery', [GalleryController::class, 'admin_index']);
Route::post('/admin/gallery/create', [GalleryController::class, 'create'])->name('add.gallery');
Route::get('/admin/gallery/edit/{id}', [GalleryController::class, 'edit']);
Route::post('/admin/gallery/update/{id}', [GalleryController::class, 'update'])->name('update.gallery');
Route::delete('/admin/gallery/delete/{id}', [GalleryController::class, 'delete'])->name('delete.gallery');

// Route for FacultyController
Route::get('/faculty', [FacultyController::class, 'index']);
Route::get('/admin/faculty', [FacultyController::class, 'admin_index']);
Route::post('/admin/faculty/create', [FacultyController::class, 'create'])->name('add.faculty');
Route::get('/admin/faculty/edit/{id}', [FacultyController::class, 'edit']);
Route::post('/admin/faculty/update/{id}', [FacultyController::class, 'update'])->name('update.faculty');
Route::delete('/admin/faculty/delete/{id}', [FacultyController::class, 'delete'])->name('delete.faculty');
