@if (isset(Auth::user()->username))
    <script>window.location = "admin/home"</script>
@endif
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="assets/icons/olla_icon.png" type="image/icon type">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>OLLA - Home</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Caveat:wght@500&display=swap');

        .gradient-custom-2 {
            /* fallback for old browsers */
            background: #4d8c4d;

            /* Chrome 10-25, Safari 5.1-6 */
            background: -webkit-linear-gradient(to right, #4d8c4d, #78a161, #A3B56B, #CDCA74);

            /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
            background: linear-gradient(to right, #4d8c4d, #78a161, #A3B56B, #CDCA74);

            font-weight: 600;
        }
        .footer-text-style {
            font-family: 'Caveat', cursive;
        }
        .carousel-inner > .item {
            height:600px;
        }
        #containItem{
            padding: 0;
        }
        .card-img-top{
            border-radius: 0;
        }
        p{
            font-size: 20px;
        }
        .card-body{
            font-family: 'Caveat', cursive;
            font-size: 25px;
        }
        .card{
            margin-top: 20px;
        }
        .font-big{
            font-size: 20px;
        }
    </style>
</head>
<header>
        <nav class="navbar navbar-expand-lg navbar-light gradient-custom-2 text-dark" class>
            <div class="container-fluid">
            @if (count($fetch) > 0)
                @foreach ($fetch as $f)
                    <a class="navbar-brand" href="/admin/home"><img src = "{{ URL('assets/icons/database_icon/'.$f->company_logo)}}" height="100" width="100" style="border-radius: 100px;"></a>    
                @endforeach
            @else
            <a class="navbar-brand" href="/admin/home"><img src = "{{ URL('assets/icons/olla_icon.png')}}" height="100" width="100" style="border-radius: 100px;"></a>
            @endif
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse font-big" id="navbarTogglerDemo02">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/">Home</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="/announcements">Announcement</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href = "/events">Events</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="/gallery">Gallery</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href = "/faculty">Faculty</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href = "/about">About</a>
                        </li>
                    </ul>
                    <form class="d-flex">
                        <a class="btn btn-success font-big" href = "{{ url('/admin') }}">ADMINISTRATOR</a>
                    </form>
                </div>
            </div>
        </nav>
    </header>
<body>
    <div class="container-fluid overflow-hidden" id="containItem">
        <!-- Carousel -->
        <div class="row">
            <div class="col-xs-12">
                <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-bs-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-bs-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-bs-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item item active">
                            <video class="img" autoplay loop muted><source src="{{ url('assets/videos/drone_shoot_1_Full HD 1080p.mp4') }}" type="video/mp4" /></video>
                            <div class="carousel-caption">
                                <h2 style="font-size: 3vw; color:yellow;">OUR LADY OF THE LILIES ACADEMY</h2>
                                <p class = "footer-text-style" style="font-size: 3vw; color:lightgreen;">A school where a child can grow and develop character values.</p>
                            </div>
                        </div>
                        <div class="carousel-item item">
                        <video class="img" autoplay loop muted><source src="{{ url('assets/videos/drone_shoot_6_Full HD 1080p.mp4') }}" type="video/mp4" /></video>
                        </div>
                        <div class="carousel-item item">
                        <video class="img" autoplay loop muted><source src="{{ url('assets/videos/drone_shoot_5_Full HD 1080p.mp4') }}" type="video/mp4" /></video>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="container p-5 text-light" style = "background-color: #3D3D3D;">
                <div class="row">
                    <h1><center><svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-clock" viewBox="0 0 16 16">
                        <path d="M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z"/>
                        <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z"/>
                        </svg> &nbsp;ANNOUNCEMENTS &nbsp;
                    </center></h1>
                </div>
            </div>
            @if (count($latest) > 0)
            <div class="container">
                <div class="row text-dark p-5">
                    @foreach ($latest as $item)
                        <div class="col-md-4">
                            <div class="card shadow">
                                <div class="card-header gradient-custom-2">
                                    <h4>{{ $item->title }}</h3>
                                </div>
                                <a href="#" class="pop">
                                    <img class = "card-img-top" src = "{{ URL('assets/images/database_image/'.$item->image) }}" height="350" width="300">
                                </a>
                                <div class="card-footer">
                                    <p>Posted at : {{ $item->created_at }}</p>
                                    <div class="d-flex">
                                    <a href = "/announcements/post/{{ $item->id }}"><button type = "button" class = "btn btn-secondary">View post</button></a>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    @endforeach       
                </div>
            </div>
            @else
            <div class="container m-5 p-5 mx-auto">
                <div class="alert alert-danger">
                    <p>No latest announcement yet!</p>
                </div>
            </div>
            @endif
        </div>
        <!-- Facilities -->
        <div class="row">
            <div class="container p-5 text-light" style = "background-color: #3D3D3D;">
                <div class="row">
                    <h1><center><svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-building" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M14.763.075A.5.5 0 0 1 15 .5v15a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5V14h-1v1.5a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1-.5-.5V10a.5.5 0 0 1 .342-.474L6 7.64V4.5a.5.5 0 0 1 .276-.447l8-4a.5.5 0 0 1 .487.022zM6 8.694 1 10.36V15h5V8.694zM7 15h2v-1.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 .5.5V15h2V1.309l-7 3.5V15z"/>
                        <path d="M2 11h1v1H2v-1zm2 0h1v1H4v-1zm-2 2h1v1H2v-1zm2 0h1v1H4v-1zm4-4h1v1H8V9zm2 0h1v1h-1V9zm-2 2h1v1H8v-1zm2 0h1v1h-1v-1zm2-2h1v1h-1V9zm0 2h1v1h-1v-1zM8 7h1v1H8V7zm2 0h1v1h-1V7zm2 0h1v1h-1V7zM8 5h1v1H8V5zm2 0h1v1h-1V5zm2 0h1v1h-1V5zm0-2h1v1h-1V3z"/>
                        </svg> &nbsp;FACILITIES &nbsp;
                    </center></h1>
                </div>
            </div>
            @if (count($post) > 0)
            <div class="container">
                <div class="row text-dark p-5">
                    @foreach ($post as $item)
                        <div class="col-md-4">
                            <div class="card shadow">
                                <div class="card-header gradient-custom-2">
                                    <h4>{{ $item->title }}</h3>
                                </div>
                                <a href="#" class="pop">
                                    <img class = "card-img-top" src = "{{ URL('assets/images/facilities/'.$item->image) }}" height="350" width="300">
                                </a>
                            </div>
                        </div>
                    @endforeach       
                    <div class="row text-dark p-3 mt-3 mx-auto">
                        {{ $post->links('pagination::bootstrap-5') }}
                    </div>
                </div>
            </div>
            @else
            <div class="container m-5 p-5 mx-auto">
                <div class="alert alert-danger">
                    <p>No facilities was posted yet!</p>
                </div>
            </div>
            @endif
        </div>
        <!-- Contact -->
        <div class="row">
            <div class="container p-5 text-light" style = "background-color: #3D3D3D;">
                <div class="row">
                    <h1><center> <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-person-rolodex" viewBox="0 0 16 16">
                    <path d="M8 9.05a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5Z"/>
                    <path d="M1 1a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h.5a.5.5 0 0 0 .5-.5.5.5 0 0 1 1 0 .5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5.5.5 0 0 1 1 0 .5.5 0 0 0 .5.5h.5a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H6.707L6 1.293A1 1 0 0 0 5.293 1H1Zm0 1h4.293L6 2.707A1 1 0 0 0 6.707 3H15v10h-.085a1.5 1.5 0 0 0-2.4-.63C11.885 11.223 10.554 10 8 10c-2.555 0-3.886 1.224-4.514 2.37a1.5 1.5 0 0 0-2.4.63H1V2Z"/>
                    </svg> &nbsp;CONTACT US</center></h1>
                </div>
            </div>
            <div class="container p-5">
                <div class="row">
                    <div class="col-sm-6 mx-auto">
                        <div class="card p-5 shadow-lg">
                            <form id = "contactForm" method = "post" action="{{ route('send.email') }}">
                                @csrf
                                @if (Session::has('internet_error'))
                                <div class="alert alert-danger">
                                        {{ Session::get('internet_error') }}
                                    </div>
                                @endif
                                @if (Session::has('message_sent'))
                                    <div class="alert alert-success">
                                        {{ Session::get('message_sent') }}
                                    </div>
                                @endif
                                @if (Session::has('error'))
                                    <div class="alert alert-danger">
                                        {{ Session::get('error') }}
                                    </div>
                                @endif
                                @if (Session::has('success'))
                                    <div class="alert alert-success">
                                        {{ Session::get('success') }}
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="name" class="col-form-label">Name:</label>
                                        <input type = "text" class = "form-control" name = "Name" id = "Name" placeholder="Name"/>
                                        @error('Name') <span class = "text-danger">{{ $message }}</span> @enderror
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="email" class="col-form-label">Email:</label>
                                        <input type = "email" class = "form-control" name = "Email" id = "Email" placeholder="Email"/>
                                        @error('Email') <span class = "text-danger">{{ $message }}</span> @enderror
                                    </div>
                                    <div class="col-sm-12">
                                        <label for="subject" class="col-form-label">Subject:</label>
                                        <input type = "text" class = "form-control" name = "Subject" id = "Subject" placeholder="Subject"/>
                                        @error('Subject') <span class = "text-danger">{{ $message }}</span> @enderror
                                    </div>
                                    <div class="col-sm-12">
                                        <label for="message" class="col-form-label">Message:</label>
                                        <textarea name = "Message" class = "form-control" rows="5" placeholder="Message"></textarea><br>
                                        @error('Message') <span class = "text-danger">{{ $message }}</span> @enderror
                                    </div>
                                    <div class="col-sm-6 mt-2">
                                        <button type = "submit" class = "form-control btn btn-primary" id = "submitBtn" name = "submitBtn">Submit</button>
                                    </div>
                                    <div class="col-sm-6 mt-2">
                                        <button type = "button" class = "form-control btn btn-secondary" id = "clearBtn" name = "clearBtn">Clear</button>
                                    </div>
                                </div>
                            </form>
                            <hr>
                            <center><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-envelope-fill" viewBox="0 0 16 16">
                            <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z"/>
                            </svg><p>liliesurdaneta@yahoo.com<br>ollaurdanetacity@gmail.com</p></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" data-dismiss="modal">
            <div class="modal-content"  >              
            <div class="modal-body">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <img src="" class="imagepreview" style="width: 100%;" >
            </div> 
            </div>
        </div>
    </div>
</body>
<footer id="footer" class="flex-shrink-0 py-4 gradient-custom-2 text-white-50 sticky-bottom">
    <div class="container text-center text-dark footer-text-style">
    <small style="font-size: 20px;">Copyright &copy{{ date("Y") }} - Our Lady of the Lilies Academy</small>
    </div>
</footer>
</html>
<script>
    $(function() {
            $('.pop').on('click', function() {
                $('.imagepreview').attr('src', $(this).find('img').attr('src'));
                $('#imagemodal').modal('show');   
            });     
    });
    $(document).on('click', '#clearBtn', function(){
        $('#contactForm')[0].reset();
    });
</script>