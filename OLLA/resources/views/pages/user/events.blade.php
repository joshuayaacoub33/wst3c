@if (isset(Auth::user()->username))
    <script>window.location = "admin/home"</script>
@endif
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="assets/icons/olla_icon.png" type="image/icon type">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>OLLA - Events</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Caveat:wght@500&display=swap');

        .gradient-custom-2 {
            /* fallback for old browsers */
            background: #4d8c4d;

            /* Chrome 10-25, Safari 5.1-6 */
            background: -webkit-linear-gradient(to right, #4d8c4d, #78a161, #A3B56B, #CDCA74);

            /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
            background: linear-gradient(to right, #4d8c4d, #78a161, #A3B56B, #CDCA74);

            font-weight: 600;
        }
        .footer-text-style {
            font-family: 'Caveat', cursive;
        }
        #containItem{
            padding: 0;
        }
        p{
            font-size: 20px;
        }
        .carousel-caption {
            transform: translateY(-50%);
            bottom: 0;
            top: 50%;
        }
        #imageCarousel{
            filter: brightness(50%);
        }
        .card-img-top {
            width: 100%;
            object-fit: cover;
        }
        .font-big{
            font-size: 20px;
        }
    </style>
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light gradient-custom-2 text-dark">
            <div class="container-fluid">
            @if (count($fetch) > 0)
                @foreach ($fetch as $f)
                <a class="navbar-brand" href="/admin/home"><img src = "{{ URL('assets/icons/database_icon/'.$f->company_logo)}}" height="100" width="100" style="border-radius: 100px;"></a>    
                @endforeach
            @else
            <a class="navbar-brand" href="/admin/home"><img src = "{{ URL('assets/icons/olla_icon.png')}}" height="100" width="100" style="border-radius: 100px;"></a>
            @endif
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse font-big" id="navbarTogglerDemo02">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="/">Home</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="/announcements">Announcement</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link active" href = "/events">Events</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="/gallery">Gallery</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href = "/faculty">Faculty</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href = "/about">About</a>
                    </li>
                </ul>
                <form class="d-flex">
                    <a class="btn btn-success font-big" href = "{{ url('/admin') }}">ADMINISTRATOR</a>
                </form>
                </div>
            </div>
        </nav>
    </header>
    <div class="container-fluid overflow-hidden" id="containItem">
        <div class="row">
            <div class="col-xs-12">
                <div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="{{ URL('assets/images/olla_church.jpg') }}" width = "100%" height = "650px;" class="d-block w-100 img-responsive" alt="olla church" id = "imageCarousel">
                            <div class="carousel-caption">
                                <h1 style="color: yellow; letter-spacing: 50px; font-size: 8vw;" class = "mt-5">EVENT</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if (count($posts) > 0)
            <div class="container">
                <div class="row text-dark p-2">
                    @foreach ($posts as $item)
                        <div class="col-md-4 mt-5">
                            <div class="card shadow">
                                <div class="card-header gradient-custom-2">
                                    <h4>{{ $item->title }}</h3>
                                </div>
                                <a href="#" class="pop">
                                    <img class = "card-img-top" src = "{{ URL('assets/images/database_image/'.$item->image) }}" height="350" width="300">
                                </a>
                                <div class="card-footer">
                                    <p>Posted at : {{ $item->created_at }}</p>
                                    <div class="d-flex">
                                    <a href = "/announcements/post/{{ $item->id }}"><button type = "button" class = "btn btn-secondary">View post</button></a>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    @endforeach       
                </div>
            </div>
            @else
            <div class="container m-5 p-5 mx-auto">
                <div class="alert alert-danger">
                    <p>No events yet!</p>
                </div>
            </div>
        </div>
    @endif
    <footer id="footer" class="flex-shrink-0 py-4 gradient-custom-2 text-white-50 sticky-bottom" style="margin-top: 50px;">
        <div class="container text-center text-dark footer-text-style">
        <small style="font-size: 20px;">Copyright &copy{{ date("Y") }} - Our Lady of the Lilies Academy</small>
        </div>
    </footer>
    <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" data-dismiss="modal">
            <div class="modal-content"  >              
            <div class="modal-body">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <img src="" class="imagepreview" style="width: 100%;" >
            </div> 
            </div>
        </div>
    </div>
</body>
</html>
<script>
    $(function() {
            $('.pop').on('click', function() {
                $('.imagepreview').attr('src', $(this).find('img').attr('src'));
                $('#imagemodal').modal('show');   
            });     
    });
</script>