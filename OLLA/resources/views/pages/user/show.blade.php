<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ asset('assets/icons/olla_icon.png') }}" type='image/x-icon'>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
    <title>OLLA - Post</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Caveat:wght@500&display=swap');

        .gradient-custom-2 {
            /* fallback for old browsers */
            background: #4d8c4d;

            /* Chrome 10-25, Safari 5.1-6 */
            background: -webkit-linear-gradient(to right, #4d8c4d, #78a161, #A3B56B, #CDCA74);

            /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
            background: linear-gradient(to right, #4d8c4d, #78a161, #A3B56B, #CDCA74);

            font-weight: 600;
        }
        .footer-text-style {
            font-family: 'Caveat', cursive;
        }

        .modal-dialog{
            overflow-y: auto;
        }

        .modal-body{
            max-height: 60vh;
            overflow-y: auto;
        }

        .custom-button{
            color: white;
            border-radius: 10px;
            padding: 6px 24px;
            font-size: 18px;
            font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            border: none;
            box-shadow: 0 4px 8px 0 #ccc;
            letter-spacing: 1.5px;
        }
        .card-horizontal {
            display: flex;
            flex: 1 1 auto;
        }
        #containItem{
            padding: 0;
        }
        .carousel-caption {
            transform: translateY(-50%);
            bottom: 0;
            top: 50%;
        }
        #imageCarousel{
            filter: brightness(50%);
        }
        .font-big{
            font-size: 20px;
        }
    </style>
</head>
<header>
    <nav class="navbar navbar-expand-lg navbar-light gradient-custom-2 text-dark">
        <div class="container-fluid">
        @if (count($fetch) > 0)
                @foreach ($fetch as $f)
                <a class="navbar-brand" href="/admin/home"><img src = "{{ URL('assets/icons/database_icon/'.$f->company_logo)}}" height="100" width="100" style="border-radius: 100px;"></a>    
                @endforeach
            @else
            <a class="navbar-brand" href="/admin/home"><img src = "{{ URL('assets/icons/olla_icon.png')}}" height="100" width="100" style="border-radius: 100px;"></a>
            @endif
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse font-big" id="navbarTogglerDemo02">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                <a class="nav-link" aria-current="page" href="/">Home</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="/announcements">Announcement</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href = "/events">Events</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="/gallery">Gallery</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href = "/faculty">Faculty</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href = "/about">About</a>
                </li>
            </ul>
            <form class="d-flex">
                <a class="btn btn-success font-big" href = "{{ url('/admin') }}">ADMINISTRATOR</a>
            </form>
            </div>
        </div>
    </nav>
</header>
<body>
    <div class="container p-3 m-3 mx-auto">
        <div class="row">
            <div class="span8">
                <h1>{{ Str::upper($post->title) }}</h1>
                <h4>{{ $post->subtitle }}</h4>
                <div style="margin-bottom: 20px;">
                    <span class="badge bg-success">Posted: {{ $post->created_at }}</span><div class="pull-right"><span class="badge bg-warning text-dark">{{ Str::upper($post->postType) }}</span></div>
                </div> 
                <a href="#" class="pop">
                    <img src="{{ URL('assets/images/database_image/'.$post->image) }}" height="250" width="400" alt="post img" class="pull-left img-responsive postImg img-thumbnail margin10">   
                </a>
                <p>{!! $post->description !!}</p>
                <hr>
                <a href="{{ url()->previous() }}"><button type = "button" class = "btn btn-secondary" style="margin-bottom: 100px;">Back</button></a>   
            </div>
        </div>
    </div>
    <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" data-dismiss="modal">
            <div class="modal-content"  >              
            <div class="modal-body">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <img src="" class="imagepreview" style="width: 100%;" >
            </div> 
            </div>
        </div>
    </div>
    <footer id="footer" class="flex-shrink-0 py-4 gradient-custom-2 text-white-50 fixed-bottom">
        <div class="container text-center text-dark footer-text-style">
            <small style="font-size: 20px;">Copyright &copy{{ date("Y") }} - Our Lady of the Lilies Academy</small>
        </div>
    </footer>
</body>
</html>
<script>
    $(function() {
            $('.pop').on('click', function() {
                $('.imagepreview').attr('src', $(this).find('img').attr('src'));
                $('#imagemodal').modal('show');   
            });     
    });
</script>