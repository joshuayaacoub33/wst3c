@if (isset(Auth::user()->username))
    <script>window.location = "admin/home"</script>
@endif
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="assets/icons/olla_icon.png" type="image/icon type">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <title>OLLA - Admin Login</title>
    <style>

      @import url('https://fonts.googleapis.com/css2?family=Caveat:wght@500&display=swap');
      .gradient-custom-2 {
        /* fallback for old browsers */
        background: #4d8c4d;

        /* Chrome 10-25, Safari 5.1-6 */
        background: -webkit-linear-gradient(to right, #4d8c4d, #78a161, #A3B56B, #CDCA74);

        /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        background: linear-gradient(to right, #4d8c4d, #78a161, #A3B56B, #CDCA74);
      }

      .footer-text-style {
        font-family: 'Caveat', cursive;
      }

      .login-button{
        color: white;
        border-radius: 10px;
        padding: 6px 24px;
        font-size: 18px;
        font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        border: none;
        box-shadow: 0 4px 8px 0 #ccc;
        letter-spacing: 1.5px;
      }
      .login-button:hover{
        color:black;
      }

      @media (min-width: 768px) {
        .gradient-form {
          height: 100vh !important;
        }
      }
      @media (min-width: 769px) {
        .gradient-custom-2 {
          border-top-right-radius: .3rem;
          border-bottom-right-radius: .3rem;
        }
      }
    </style>
</head>
<body>
<section class="h-100 gradient-form" style="background-color: #eee;">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-xl-10">
        <div class="card rounded-3 text-black">
          <div class="row g-0">
            <div class="col-lg-6">
              <div class="card-body p-md-5 mx-md-4">

                <div class="text-center">
                  <img src="{{ URL('assets/icons/olla_icon.png') }}" style="width: 70px;" alt="logo">
                  <h4 class="mt-1 mb-5 pb-1">OLLA Admin</h4>

                  @if (isset(Auth::user()->username))
                    <script>window.location = "admin/home"</script>
                  @endif
                  @if ($message = Session::get('error'))
                  <div class="alert alert-danger alert-block">
                    {{ $message }}
                  </div>
                    
                  @endif
                  @if (count($errors) > 0)
                    <div class="alert alert-danger">
                      <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                      </ul>
                    </div>
                  @endif
                </div>
                <form method = "post" action="{{ url('/admin/checkLogin') }}">
                  {{ csrf_field() }}
                  <div class="form-outline mb-4">
                    <label class="form-label" for="formLabelUsername">Username:</label>
                    <input type="text" id="username" class="form-control"
                      placeholder="Username" name ="username"/>
                  </div>

                  <div class="form-outline mb-4">
                    <label class="form-label" for="formLabelPassword">Password:</label>
                    <input type="password" id="password" class="form-control" placeholder="Password"
                    name = "password"/>
                  </div>

                  <div class="text-center pt-1 mb-5 pb-1">
                    <input class="btn btn-success btn-block fa-lg login-button mb-3" type="submit" id = "Login" name = "Login" value="LOGIN">
                  </div>
                </form>

              </div>
            </div>
            <div class="col-lg-6 d-flex align-items-center gradient-custom-2">
              <div class="text-black px-3 py-4 p-md-5 mx-md-4">
                <h2 class="mb-4 footer-text-style">We are more than just a school</h2>
                <p class="small mb-0" style = "font-weight:600;">If you are aiming for excellence that will inspire your child to be the best and to unlock their dreams, 
                  Our Lady of the Lilies Academy is here to provide a child-friendly environment while providing 
                  them a quality education that will help them to strive for the best.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</body>
</html>