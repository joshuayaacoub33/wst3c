@if (!isset(Auth::user()->username))
    <script>window.location = "/admin"</script>
@endif
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ asset('assets/icons/olla_icon.png') }}" type='image/x-icon'>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css" rel="stylesheet">
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
    <title>OLLA - Admin Gallery</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Caveat:wght@500&display=swap');

        .gradient-custom-2 {
            /* fallback for old browsers */
            background: #4d8c4d;

            /* Chrome 10-25, Safari 5.1-6 */
            background: -webkit-linear-gradient(to right, #4d8c4d, #78a161, #A3B56B, #CDCA74);

            /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
            background: linear-gradient(to right, #4d8c4d, #78a161, #A3B56B, #CDCA74);

            font-weight: 600;
        }
        .footer-text-style {
            font-family: 'Caveat', cursive;
        }

        .modal-dialog{
            overflow-y: auto;
        }

        .modal-body{
            max-height: 60vh;
            overflow-y: auto;
        }

        .custom-button{
            color: white;
            border-radius: 10px;
            padding: 6px 24px;
            font-size: 18px;
            font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            border: none;
            box-shadow: 0 4px 8px 0 #ccc;
            letter-spacing: 1.5px;
        }
        #containItem{
            padding: 0;
        }
        p{
            font-size: 20px;
        }
        .carousel-caption {
            transform: translateY(-50%);
            bottom: 0;
            top: 50%;
        }
        #imageCarousel{
            filter: brightness(50%);
        }
        .card-horizontal {
            display: flex;
            flex: 1 1 auto;
        }
        a{
            text-decoration: none;
        }
        .font-big{
            font-size: 20px;
        }
    </style>
</head>
<header>
    <nav class="navbar navbar-expand-lg navbar-light gradient-custom-2 text-dark">
        <div class="container-fluid">
        @if (count($fetch) > 0)
                @foreach ($fetch as $f)
                <a class="navbar-brand" href="/admin/home"><img src = "{{ URL('assets/icons/database_icon/'.$f->company_logo)}}" height="100" width="100" style="border-radius: 100px;"></a>    
                @endforeach
            @else
            <a class="navbar-brand" href="/admin/home"><img src = "{{ URL('assets/icons/olla_icon.png')}}" height="100" width="100" style="border-radius: 100px;"></a>
            @endif
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse font-big" id="navbarTogglerDemo02">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <a class="nav-link" aria-current="page" href="/admin/home">Home</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="/admin/announcements">Announcement</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href = "/admin/events">Events</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link active" href="/admin/gallery">Gallery</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href = "/admin/faculty">Faculty</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href = "/admin/about">About</a>
                    </li>
                </ul>
                <form class="d-flex">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item dropdown">
                            @if (isset(Auth::user()->username))
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Welcome, {{ Auth::user()->username }}
                            </a>
                            <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                                <li><a class="dropdown-item" href="/admin/manage-user">Manage User</a></li>
                                <li><a class="dropdown-item" href="" data-bs-toggle="modal" data-bs-target="#logoModal">Manage Logo</a></li>
                                <li><a class="dropdown-item" href="" data-bs-toggle="modal" data-bs-target="#settingsModal">Settings</a></li>
                                <li><a class="dropdown-item" href="/admin/logout">Logout</a></li> 
                            </ul>
                            @endif
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </nav>
</header>
<body>
<div class="container-fluid overflow-hidden" id="containItem">
    <div class="row">
        <div class="col-xs-12">
            <div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="{{ URL('assets/images/olla_church.jpg') }}" width = "100%" height = "650px;" class="d-block w-100 img-responsive" alt="olla church" id = "imageCarousel">
                        <div class="carousel-caption">
                            <h1 style="color: yellow; letter-spacing: 50px; font-size: 8vw;" class = "mt-5">GALLERY</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container m-3 p-3 mx-auto">
    <button class="btn btn-success" data-bs-target="#addGalleryModal" data-bs-toggle="modal">
        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
        <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
        </svg>
    </button>
</div>
<!-- @if (count($gallery) > 0)
    <div class="container m-3 p-3 mx-auto">
        <div class="row mx-auto">
            @foreach ($gallery as $post)
            <div class="col-sm-4 mt-5">
                <div class="card shadow" style="width: 18rem;">
                    <div class="card-header">
                    <a href="#" class="pop">
                        <img class="card-img-top" src="{{ URL('assets/images/gallery/'.$post->image) }}" height="250" width="400" alt="Card image cap">
                    </a>
                    </div>
                    <div class="card-body">
                        <p class="card-text">{!! $post->description !!}</p>
                    </div>
                    <div class="card-footer">
                        <a href="/admin/gallery/edit/{{ $post->id }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-pencil-square text-primary" viewBox="0 0 16 16">
                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                            </svg> 
                        </a>
                        <a href="#" 
                            data-id="{{ $post->id}} "
                            class="delete" 
                            data-toggle="modal" 
                            data-target="#deleteModal">
                            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-trash3-fill text-danger" viewBox="0 0 16 16">
                                <path d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <br>
        {{ $gallery->links('pagination::bootstrap-5') }}
    </div>
@endif

@if (count($gallery) == 0)
<div class="container m-3 p-3 mx-auto">
    <div class="alert alert-danger">
        <p>No gallery was posted yet!</p>
    </div>
</div>
@endif -->

<div class="container overflow-hidden mx-auto p-5">
    <div class="row overflow-hidden">
        <div class="col-sm-12 mx-auto overflow-hidden">
            <table id="gallery" class="table table-striped table-bordered dt-responsive" style="width:100%">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Description</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($gallery as $post)
                    <tr>
                        <td>
                            <a href="#" class="pop">
                                <img src="{{ URL('assets/images/gallery/'.$post->image) }}" height="200" width="200" alt="Card image cap">
                            </a>
                        </td>
                        <td>{!! Str::limit($post->description, 100) !!}</td>
                        <td>
                            <a href="/admin/gallery/edit/{{ $post->id }}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-pencil-square text-primary" viewBox="0 0 16 16">
                                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                </svg> 
                            </a>
                            <a href="#" 
                                data-id="{{ $post->id }} "
                                class="delete" 
                                data-toggle="modal" 
                                data-target="#deleteModal">
                                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-trash3-fill text-danger" viewBox="0 0 16 16">
                                    <path d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z"/>
                                </svg>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Delete Gallery Modal -->
<div class="modal modal-danger fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="Delete" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Image</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form action="{{ route('delete.gallery', 'id') }}" method="post">
                @csrf
                @method('DELETE')
                <input id="id" name="id" hidden>
                <h5 class="text-center">Are you sure you want to delete this image?</h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn btn-danger">Yes, Delete</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Gallery Modal -->
<div class="modal fade" id="addGalleryModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" style="overflow: auto">
    <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Add Gallery</h3>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" onclick="resetGallery()"></button>
            </div>
            <form id = "addGalleryForm" method = "post">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 mb-3">
                            <div class="alert alert-danger print-error-msg2" style="display:none;" id = "showErrorMessage2">
                                <ul style="text-align: justify;"></ul>
                            </div>
                            <div class="alert alert-success" style="display:none;" id = "showSuccessMessage2">
                                <p><center>Image was successfully added!</center></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3 mx-auto">
                            <label>Description: </label>
                            <textarea class="form-control" name = "gallery_description" id = "gallery_description" placeholder="Description"></textarea>
                        </div>
                        <div class="col-sm-12 mb-3 mx-auto">
                            <input type="file" class="form-control" onchange="loadFileGallery(event)" name = "gallery_image" id = "gallery_image">
                            <hr>
                            <center><img id="output_gallery" width="200" class = "img-responsive"/></center>
                        </div>
                        <br>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success custom-button" id = "addGallery" name = "addGallery">ADD</button>
                    <button type="button" class="btn btn-secondary custom-button" data-bs-dismiss="modal" onclick="resetGallery()">CLOSE</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Settings Modal -->
<div class="modal fade" id="settingsModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" style="overflow: auto">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Settings</h3>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" onclick="resetForm()"></button>
      </div>
      <form id = "updateForm">
            @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <div class="alert alert-danger print-error-msg" style="display:none;" id = "showErrorMessage">
                            <ul style="text-align: justify;"></ul>
                        </div>
                        <div class="alert alert-success" style="display:none;" id = "showSuccessMessage">
                            <p><center>Password was successfully updated!</center></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <input type="hidden" id="user_id" name="user_id" value="">
                    <div class="col-sm-12 mb-3 mx-auto">
                        
                        <label for = "old-pass-label">Old Password: </label>
                        <input type = "password" class = "form-control" name = "old_password" placeholder = "Old Password" id = "old_password"/>
                    </div>
                    <div class="col-sm-12 mb-3 mx-auto">
                        <label for = "password-label">Password: </label>
                        <input type = "password" class = "form-control" name = "new_password" placeholder = "Password" id = "new_password"/>
                    </div>
                    <br><br>
                    <div class="col-sm-12 mb-3 mx-auto">
                        <label for = "passwordConfirm-label">Confirm Password: </label>
                        <input type = "password" class = "form-control" name = "new_confirm_password" placeholder = "Confirm Password" id = "new_confirm_password"/>
                    </div>
                    <br>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success custom-button" id = "update" name = "update">UPDATE</button>
                <button type="button" class="btn btn-secondary custom-button" data-bs-dismiss="modal" onclick="resetForm()">CLOSE</button>
            </div>
        </form>
    </div>
  </div>
</div>
<!-- Logo Modal -->
<div class="modal fade" id="logoModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" style="overflow: auto">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Update Logo</h3>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" onclick="resetForm()"></button>
      </div>
            <form id = "updateLogoForm" enctype="multipart/form-data" method="post">
            @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <div class="alert alert-danger print-error-msg1" style="display:none;" id = "showErrorMessage1">
                            <ul style="text-align: justify;"></ul>
                        </div>
                        <div class="alert alert-success" style="display:none;" id = "showSuccessMessage1">
                            <p><center>Logo was successfully updated!</center></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3 mx-auto">
                        <input type="file" class="form-control" onchange="loadFile(event)" name = "logo" id = "logo">
                        <hr>
                        <center><img id="output" width="200" class = "img-responsive"/></center>
                        <!-- <label for = "image-label">Image:</label>
                        <center>
                        <img src = "{{ URL('assets/icons/olla_icon.png')}}" height="200" width="210" style="border-radius: 100px;">
                        </center>
                        <input type = "file" class = "form-control" name = "logo" id = "logo_image"/> -->
                    </div>
                    <br>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success custom-button" id = "updateLogo" name = "updateLogo">UPDATE</button>
                <button type="button" class="btn btn-secondary custom-button" data-bs-dismiss="modal" onclick="resetLogo()">CLOSE</button>
            </div>
        </form>
    </div>
  </div>
</div>

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" data-dismiss="modal">
    <div class="modal-content"  >              
      <div class="modal-body">
      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" onclick="resetFacility()"></button>
        <img src="" class="imagepreview" style="width: 100%;" >
      </div> 
    </div>
  </div>
</div>
</body>

<script src="/ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace('gallery_description');
</script>

<footer id="footer" class="flex-shrink-0 py-4 gradient-custom-2 text-white-50 sticky-bottom">
    <div class="container text-center text-dark footer-text-style">
        <small style="font-size: 20px;">Copyright &copy{{ date("Y") }} - Our Lady of the Lilies Academy</small>
    </div>
</footer>
</html>

<script>
    $(document).ready(function () {
        $('#gallery').DataTable({
            select: true,
            scrollX: true,
        });
    });

    $(function() {
            $('.pop').on('click', function() {
                $('.imagepreview').attr('src', $(this).find('img').attr('src'));
                $('#imagemodal').modal('show');   
            });     
    });
     $(document).on('click','.delete',function(){
        let id = $(this).attr('data-id');
        $('#id').val(id);
        $('#deleteModal').modal('show');
    });

    var loadFile = function(event) {
	    var image = document.getElementById('output');
	    image.src = URL.createObjectURL(event.target.files[0]);
    };

    var loadFileGallery = function(event) {
	    var image = document.getElementById('output_gallery');
	    image.src = URL.createObjectURL(event.target.files[0]);
    };
    function resetEditForm()
    {
        document.getElementById("editPostForm").reset();
        for(instance in CKEDITOR.instances){
            CKEDITOR.instances[instance].setData('');
        }
    }

    function resetForm()
    {
        document.getElementById("updateForm").reset();
    }

    function clearPostForm()
    {
        document.getElementById("announcementForm").reset();
        for(instance in CKEDITOR.instances){
            CKEDITOR.instances[instance].setData('');
        }
    }

    function resetGallery()
    {
        for(instance in CKEDITOR.instances){
            CKEDITOR.instances[instance].setData('');
        }
    }

    $(document).on('click', '#update', function(e){
        // $("#changePassword").attr("disabled", true);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var new_password = $('#new_password').val();
        var old_password = $('#old_password').val();
        var new_confirm_password = $('#new_confirm_password').val();

        $.ajax({
            type: 'post',
            url: "{{ route('change.password') }}",
            data:{
                old_password:old_password,
                new_password:new_password,
                new_confirm_password:new_confirm_password
            },
            cache: false,
            success: function (data){

                if($.isEmptyObject(data.error)){
                    $('#updateForm')[0].reset();
                    $('#showSuccessMessage').show().delay(2500).fadeOut();
                }
                else{
                    $('#updateForm')[0].reset();
                    $('#showErrorMessage').show().delay(2500).fadeOut();
                    printErrorMsg(data.error);
                }

            }
        });
    });

    function printErrorMsg (msg) {
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display','block');
        $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
        });
    }
    $(document).on('click', '#updateLogo', function(e){
        // $("#changePassword").attr("disabled", true);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let formData = new FormData($('#updateLogoForm')[0]);

        $.ajax({
            type: 'post',
            url: "{{ route('update.logo') }}",
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success: function (data){
                if($.isEmptyObject(data.error)){
                    $('#updateLogoForm')[0].reset();
                    $('#showSuccessMessage1').show().delay(2500).fadeOut();
                }
                else{
                    $('#updateLogoForm')[0].reset();
                    $('#showErrorMessage1').show().delay(2500).fadeOut();
                    printErrorMsg1(data.error);
                }

            }
        });
    });

    function printErrorMsg1 (msg) {
        $(".print-error-msg1").find("ul").html('');
        $(".print-error-msg1").css('display','block');
        $.each( msg, function( key, value ) {
            $(".print-error-msg1").find("ul").append('<li>'+value+'</li>');
        });
    }

    $('#addGalleryForm').submit(function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        const formData = new FormData(this);
        
        $.ajax({
            method: 'post',
            url: "{{ route('add.gallery') }}",
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success: function (data){
                resetGallery();
                if($.isEmptyObject(data.error)){
                    $('#showSuccessMessage2').show().delay(2500).fadeOut();
                }
                else{
                    resetGallery();
                    $('#showErrorMessage2').show().delay(2500).fadeOut();
                    printErrorMsg2(data.error);
                }
            }
        });
    });

    function printErrorMsg2 (msg) {
        $(".print-error-msg2").find("ul").html('');
        $(".print-error-msg2").css('display','block');
        $.each( msg, function( key, value ) {
            $(".print-error-msg2").find("ul").append('<li>'+value+'</li>');
        });
    }
</script>
</html>