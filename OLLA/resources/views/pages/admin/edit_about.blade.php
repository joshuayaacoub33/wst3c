@if (!isset(Auth::user()->username))
    <script>window.location = "/admin"</script>
@endif
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ asset('assets/icons/olla_icon.png') }}" type='image/x-icon'>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
    <title>OLLA - Admin About Edit</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Caveat:wght@500&display=swap');

        .gradient-custom-2 {
            /* fallback for old browsers */
            background: #4d8c4d;

            /* Chrome 10-25, Safari 5.1-6 */
            background: -webkit-linear-gradient(to right, #4d8c4d, #78a161, #A3B56B, #CDCA74);

            /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
            background: linear-gradient(to right, #4d8c4d, #78a161, #A3B56B, #CDCA74);

            font-weight: 600;
        }
        .footer-text-style {
            font-family: 'Caveat', cursive;
        }
        .custom-button{
            color: white;
            border-radius: 10px;
            padding: 6px 24px;
            font-size: 18px;
            font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            border: none;
            box-shadow: 0 4px 8px 0 #ccc;
            letter-spacing: 1.5px;
        }
        .font-big{
            font-size: 20px;
        }
    </style>
</head>
<header>
    <nav class="navbar navbar-expand-lg navbar-light gradient-custom-2 text-dark">
        <div class="container-fluid">
            @if (count($fetch) > 0)
                @foreach ($fetch as $f)
                <a class="navbar-brand" href="/admin/home"><img src = "{{ URL('assets/icons/database_icon/'.$f->company_logo)}}" height="100" width="100" style="border-radius: 100px;"></a>    
                @endforeach
            @else
            <a class="navbar-brand" href="/admin/home"><img src = "{{ URL('assets/icons/olla_icon.png')}}" height="100" width="100" style="border-radius: 100px;"></a>
            @endif
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse font-big" id="navbarTogglerDemo02">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="/admin/home">Home</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="/admin/announcements">Announcement</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href = "/admin/events">Events</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="/admin/gallery">Gallery</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href = "/admin/faculty">Faculty</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href = "/admin/about">About</a>
                    </li>
                </ul>
                <form class="d-flex">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item dropdown">
                            @if (isset(Auth::user()->username))
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Welcome, {{ Auth::user()->username }}
                            </a>
                            <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                                <li><a class="dropdown-item" href="/admin/manage-user">Manage User</a></li>
                                <li><a class="dropdown-item" href="" data-bs-toggle="modal" data-bs-target="#logoModal">Manage Logo</a></li>    
                                <li><a class="dropdown-item" href="" data-bs-toggle="modal" data-bs-target="#settingsModal">Settings</a></li>
                                <li><a class="dropdown-item" href="/admin/logout">Logout</a></li> 
                            </ul>
                            @endif
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </nav>
</header>
<body>
<form id = "editFacilitiesForm" enctype="multipart/form-data" method="post" action="@if (isset($post->id)) {{ route('update.about', $post->id )  }}@endif">
    {{ csrf_field() }}
    <div class="card-body">
        @if (count($errors) > 0)
            <div class="row">
                <div class="col-sm-8 mt-3 mx-auto">
                    <div class="alert alert-danger">
                        <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        @if(session()->has('update'))
            <div class="row">
                <div class="col-sm-8 mt-3 mx-auto">
                    <div class="alert alert-success">
                        {{ session()->get('update') }}
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-8 mb-3 mx-auto">
                <h1>Edit About</h1>
            </div>
            @if(isset($post->id))
            <input type="hidden" id="post_id" name="post_id" value="{{ $post->id }}">
            <div class="col-sm-8 mb-3 mx-auto">
                <label>Description: </label>
                <textarea rows="5" class = "form-control" placeholder = "Description" name = "description" id = "description">
                    {{ $post->description }}</textarea>
            </div>
            <div class="col-sm-8 mb-3 mx-auto" style="margin-top: 100px;">
                <div class="card-footer">
                    <button type="submit" class="btn btn-success custom-button" id = "updatePost" name = "updatePost">UPDATE</button>
                    <a href = "/admin/about"><button type="button" class="btn btn-secondary custom-button">BACK</button></a>
                </div>
            </div>
            @endif
        </div>
    </div>
</form>
<!-- Settings Modal -->
<div class="modal fade" id="settingsModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" style="overflow: auto">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Settings</h3>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" onclick="resetForm()"></button>
      </div>
      <form id = "updateForm">
            @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <div class="alert alert-danger print-error-msg" style="display:none;" id = "showErrorMessage">
                            <ul style="text-align: justify;"></ul>
                        </div>
                        <div class="alert alert-success" style="display:none;" id = "showSuccessMessage">
                            <p><center>Password was successfully updated!</center></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <input type="hidden" id="user_id" name="user_id" value="">
                    <div class="col-sm-12 mb-3 mx-auto">
                        
                        <label for = "old-pass-label">Old Password: </label>
                        <input type = "password" class = "form-control" name = "old_password" placeholder = "Old Password" id = "old_password"/>
                    </div>
                    <div class="col-sm-12 mb-3 mx-auto">
                        <label for = "password-label">Password: </label>
                        <input type = "password" class = "form-control" name = "new_password" placeholder = "Password" id = "new_password"/>
                    </div>
                    <br><br>
                    <div class="col-sm-12 mb-3 mx-auto">
                        <label for = "passwordConfirm-label">Confirm Password: </label>
                        <input type = "password" class = "form-control" name = "new_confirm_password" placeholder = "Confirm Password" id = "new_confirm_password"/>
                    </div>
                    <br>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success custom-button" id = "update" name = "update">UPDATE</button>
                <button type="button" class="btn btn-secondary custom-button" data-bs-dismiss="modal" onclick="resetForm()">CLOSE</button>
            </div>
        </form>
    </div>
  </div>
</div>

<!-- Logo Modal -->
<div class="modal fade" id="logoModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" style="overflow: auto">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Update Logo</h3>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" onclick="resetForm()"></button>
      </div>
            <form id = "updateLogoForm" enctype="multipart/form-data" method="post">
            @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <div class="alert alert-danger print-error-msg1" style="display:none;" id = "showErrorMessage1">
                            <ul style="text-align: justify;"></ul>
                        </div>
                        <div class="alert alert-success" style="display:none;" id = "showSuccessMessage1">
                            <p><center>Logo was successfully updated!</center></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3 mx-auto">
                        <input type="file" class="form-control" onchange="loadFile(event)" name = "logo" id = "logo">
                        <hr>
                        <center><img id="output" width="200" class = "img-responsive"/></center>
                        <!-- <label for = "image-label">Image:</label>
                        <center>
                        <img src = "{{ URL('assets/icons/olla_icon.png')}}" height="200" width="210" style="border-radius: 100px;">
                        </center>
                        <input type = "file" class = "form-control" name = "logo" id = "logo_image"/> -->
                    </div>
                    <br>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success custom-button" id = "updateLogo" name = "updateLogo">UPDATE</button>
                <button type="button" class="btn btn-secondary custom-button" data-bs-dismiss="modal" onclick="resetLogo()">CLOSE</button>
            </div>
        </form>
    </div>
  </div>
</div>
<script>
    CKEDITOR.replace('description');
    var loadFile = function(event) {
	    var image = document.getElementById('output');
	    image.src = URL.createObjectURL(event.target.files[0]);
    };

    function resetForm()
    {
        document.getElementById("updateForm").reset();
    }

    function clearPostForm()
    {
        document.getElementById("announcementForm").reset();
        for(instance in CKEDITOR.instances){
            CKEDITOR.instances[instance].setData('');
        }
    }

    $(document).on('click', '#update', function(e){
        // $("#changePassword").attr("disabled", true);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var new_password = $('#new_password').val();
        var old_password = $('#old_password').val();
        var new_confirm_password = $('#new_confirm_password').val();

        $.ajax({
            type: 'post',
            url: "{{ route('change.password') }}",
            data:{
                old_password:old_password,
                new_password:new_password,
                new_confirm_password:new_confirm_password
            },
            cache: false,
            success: function (data){

                if($.isEmptyObject(data.error)){
                    $('#updateForm')[0].reset();
                    $('#showSuccessMessage').show().delay(2500).fadeOut();
                }
                else{
                    $('#updateForm')[0].reset();
                    $('#showErrorMessage').show().delay(2500).fadeOut();
                    printErrorMsg(data.error);
                }

            }
        });
    });

    function printErrorMsg (msg) {
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display','block');
        $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
        });
    }
    $(document).on('click', '#updateLogo', function(e){
        // $("#changePassword").attr("disabled", true);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let formData = new FormData($('#updateLogoForm')[0]);

        $.ajax({
            type: 'post',
            url: "{{ route('update.logo') }}",
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success: function (data){
                if($.isEmptyObject(data.error)){
                    $('#updateLogoForm')[0].reset();
                    $('#showSuccessMessage1').show().delay(2500).fadeOut();
                }
                else{
                    $('#updateLogoForm')[0].reset();
                    $('#showErrorMessage1').show().delay(2500).fadeOut();
                    printErrorMsg1(data.error);
                }

            }
        });
    });

    function printErrorMsg1 (msg) {
        $(".print-error-msg1").find("ul").html('');
        $(".print-error-msg1").css('display','block');
        $.each( msg, function( key, value ) {
            $(".print-error-msg1").find("ul").append('<li>'+value+'</li>');
        });
    }
</script>
<footer id="footer" class="flex-shrink-0 py-4 gradient-custom-2 text-white-50 sticky-bottom">
    <div class="container text-center text-dark footer-text-style">
        <small style="font-size: 20px;">Copyright &copy{{ date("Y") }} - Our Lady of the Lilies Academy</small>
    </div>
</footer>
</body>
</html>