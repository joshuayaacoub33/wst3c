@component('mail::message')

From: {{ $details['fromEmail'] }}

Message: {{ $details['body'] }}

@component('mail::button', ['url' => 'https://www.facebook.com/Our-Lady-of-the-Lilies-Academy-Page-111038620679453'])
Visit Official Page
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent