<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <title>Internal Server Error</title>
    <style>
        .gradient-custom-2 {
            /* fallback for old browsers */
            background: #4d8c4d;

            /* Chrome 10-25, Safari 5.1-6 */
            background: -webkit-linear-gradient(to right, #4d8c4d, #78a161, #A3B56B, #CDCA74);

            /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
            background: linear-gradient(to right, #4d8c4d, #78a161, #A3B56B, #CDCA74);
        }
        * {
            font-family: sans-serif;
            color: rgba(0,0,0,0.75);
        }
        body {
            margin: 0;
            display: flex;
            flex-direction: column;
            justify-content: center;
            height: 100vh;
            padding: 0px 30px;
            background: #ddd;
        }

        .wrapper {
            max-width: 960px;
            width: 100%; 
            margin: 30px auto;
            transform: scale(0.8);
        }
        .landing-page {
            max-width: 960px;
            height: 475px;
            margin: 0;
            box-shadow: 0px 0px 8px 1px #ccc;
            background: #fafafa;
            border-radius: 8px;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
        svg {
            width: 50%;
            max-height: 225px;
            height: auto;
            margin: 0 0 15px;
        }
        h1 {
            font-size: 48px;
            margin: 0;
        }
        p {
            font-size: 18px;
            width: 35%; 
            margin: 16px auto 24px;
            text-align: center;
        }
        a{
            color: black;
            border-radius: 50px;
            padding: 8px 24px;
            font-size: 18px;
            background: #62AD9B;
            border: none;
            box-shadow: 0 4px 8px 0 #ccc;
        }
        a.gradient-custom-2{
            text-decoration: none;
        }

        a.gradient-custom-2:hover{
            color: black;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="landing-page">
            <div style="text-align:center;" class="icon__download">
                <img src = "{{ URL('assets/icons/olla_icon.png') }}" height="150" width="150">
            </div>
            
            <h1><b>500 Error.</b></h1>
            <p> The request was not completed due to an internal error on the server side. </p>
        </div>
    </div>
</body>
</html>