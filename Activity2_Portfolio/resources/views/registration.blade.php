<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Title bar icon -->
    <link rel="icon" href="{{ URL('images/favicon.png') }}" type="image/icon type">

    <!-- CSS and JS Libraries -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <style>
        header {
            height:0vh;
            min-height:400px;
            background-size:cover;
            background-color: black;
        }

        .hidden-spacer {
            background-color: black;
            height: 56px
        }
        label{
            font-weight: 600;
            font-size: 18px;
        }
    </style>
    <title>Registration</title>
</head>
<body>
<header>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <center>
                    <div style = "color:white; margin-top: 140px; font-size: 70px; letter-spacing:20px;">REGISTER</div></br>
                    <div style = "color:white; font-size: 18px;">REGISTER HERE TO GAIN KNOWLEDGE</div>
                </center> 
            </div>
        </div>
    </div>
</header>
<nav class="navbar navbar-expand-sm fixed-top navbar-dark">
    <div class="container">
        <a class="navbar-brand" href="/">Home</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar1">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar1">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="/about">About me</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/registration">Registration</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/login">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/gallery">Gallery</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="sticky-top bg-black hidden-spacer"> </div>
<div class="container pt-5">
        <div class="row">
            <div class="col-12">
                <div class="card" style = "border-radius: 10px;">
                    <div class="card-header text-light" style = "background-color:black;
                    border-top-left-radius:10px; border-top-right-radius:10px;">
                        <h2>Fill Up Information</h2>
                    </div>
                    <div class="card-body">
                        <div class="row m-3">
                            <div class="col-sm-6">
                                <label>First Name:</label>
                                <input type = "text" class = "form-control" placeholder = "First Name" required/>
                            </div>
                            <div class="col-sm-6">
                                <label>Last Name:</label>
                                <input type = "text" class = "form-control" placeholder = "Last Name" required/>
                            </div>
                        </div>
                        <div class="row m-3">
                            <div class="col-sm-6">
                                <label>Address:</label>
                                <input type = "text" class = "form-control" placeholder = "Address" required/>
                            </div>
                            <div class="col-sm-6">
                                <label>ZIP/Postal Code:</label>
                                <input type = "text" class = "form-control" placeholder = "ZIP/Postal" required/>
                            </div>
                        </div>
                        <div class="row m-3">
                            <div class="col-sm-6">
                                <label>Date of Birth:</label>
                                <input type = "date" class = "form-control" placeholder = "First Name" required/>
                            </div>
                            <div class="col-sm-6">
                                <label>Gender</label>
                                <select class = "form-select" required>
                                    <option value = "Male">Male</option>
                                    <option value = "Female">Female</option>
                                    <option value = "Other">Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="row m-3">
                            <div class="col-sm-6">
                                <label>Username</label>
                                <input type = "text" class = "form-control" placeholder = "Username" required/>
                            </div>
                            <div class="col-sm-6">
                                <label>Password</label>
                                <input type = "password" class = "form-control" placeholder = "Password" required/>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a class = "btn btn-primary" href = "/registration" style = "padding: 10px 50px; font-size: 18px; float:right;">Register</a>
                        <a href = "/login" style = "padding: 10px 20px; font-size: 18px; float:right; text-decoration: none;">Already have an account? Login here</a>          
                    </div>
                </div>
            </div>
        </div>
</div>
<!--container-->
<br><br>
</body>
<footer class="page-footer font-small" style = "background-color: black; color:white;">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2022 Copyright:
    <a href="/" style = "text-decoration:none; color:white;"> Joshua Maurice Yaacoub</a>
  </div>
  <!-- Copyright -->
</footer>
</html>