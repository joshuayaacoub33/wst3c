<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Title bar icon -->
    <link rel="icon" href="{{ URL('images/favicon.png') }}" type="image/icon type">

    <!-- CSS and JS Libraries -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    
    <style>
        header {
            height: 0vh;
            padding-top: 50px;
            min-height:420px;
            background-size:cover;
            background-color: black;
        }
        .hidden-spacer {
            background-color: black;
            height: 56px
        }
        img{
            margin-top: 20px;
        }
    </style>
    <title>Homepage</title>
</head>
<body>
<header>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div style = "color:white; padding: 50px; font-size: 50px;">
                    <center>Want to know about me more?</center>
                </div>
                <center>
                    <div class="col-sm-6">
                        <a href = "/about" class = "btn btn-primary" style = "padding: 5px 105px;">About Me</a>   
                    </div>
                </center>
            </div>
        </div>
    </div>
</header>
<nav class="navbar navbar-expand-sm fixed-top navbar-dark">
    <div class="container">
        <a class="navbar-brand" href="/">Home</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar1">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar1">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="/about">About me</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/registration">Registration</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/login">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/gallery">Gallery</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="sticky-top bg-black hidden-spacer"> </div>
<br><br>
<h1><center>Follow my Social Media</center></h1>
<br><br>
<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <div class="card shadow-lg p-3 mb-5">
                <div class="card-top img">
                    <center><img src = "{{ URL('images/image_index6.png') }}" height = "200" width = "200"></center> 
                </div>
                <div class="card-body">
                    <center><h5>Add me on my Facebook Account</h5></center>
                    <center><a href = "https://www.facebook.com/joshua.yaacoub33/" class = "btn btn-primary"
                    style = "padding: 5px 50px;">Add</a></center>
                </div>        
            </div>
        </div> 
        <div class="col-sm-4">
            <div class="card shadow-lg p-3 mb-5">
                <div class="card-top img">
                    <center><img src = "{{ URL('images/image_index7.png') }}" height = "200" width = "200"></center> 
                </div>
                <div class="card-body">
                    <center><h5>Email me at google</h5></center>
                    <center><a href = "mailto:joshuayaacoub33@gmail.com" class = "btn btn-success"
                    style = "padding: 5px 50px;">Email</a></center>
                </div>        
            </div>
        </div> 
        <div class="col-sm-4">
            <div class="card shadow-lg p-3 mb-5">
                <div class="card-top img">
                    <center><img src = "{{ URL('images/image_index8.png') }}" height = "200" width = "200"></center> 
                </div>
                <div class="card-body">
                    <center><h5>Subscribe to my Youtube</h5></center>
                    <center><a href = "https://www.youtube.com/channel/UC1WGBeTC5GmqKDLMgx8VQkg" class = "btn btn-danger"
                    style = "padding: 5px 50px;">Subscribe</a></center>
                </div>        
            </div>
        </div> 
    </div>
</div>

<br><br><br>


</body>
<footer class="page-footer font-small" style = "background-color: black; color:white;">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2022 Copyright:
    <a href="/" style = "text-decoration:none; color:white;"> Joshua Maurice Yaacoub</a>
  </div>
  <!-- Copyright -->
</footer>
</html>