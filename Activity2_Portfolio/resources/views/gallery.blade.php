<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Title bar icon -->
    <link rel="icon" href="{{ URL('images/favicon.png') }}" type="image/icon type">

    <!-- CSS and JS Libraries -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <style>
        header {
            height:0vh;
            min-height:400px;
            background-size:cover;
            background-color: black;
        }

        .hidden-spacer {
            background-color: black;
            height: 56px
        }
        img{
            margin-top: 20px;
            border: 5px solid black;
        }
    </style>
    <title>Gallery</title>
</head>
<body>
<header>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <center>
                        <div style = "color:white; margin-top: 140px; font-size: 70px; letter-spacing:20px;">GALLERY</div>
                        <div style = "color:white; font-size: 18px;">MEMORIES AND PICTURES</div>
                    </center> 
                </div>
            </div>
        </div>
</header>
<nav class="navbar navbar-expand-sm fixed-top navbar-dark">
    <div class="container">
        <a class="navbar-brand" href="/">Home</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar1">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar1">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="/about">About me</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/registration">Registration</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/login">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/gallery">Gallery</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="sticky-top bg-black; hidden-spacer"> </div>
<div class="container pt-5">
    <div class="row">
        <div class="col-sm-4">
            <center><img src = "{{ URL('images/image_index1.jpg') }}" height="300" width="300" style = "border-radius: 150px;"></center>
            
        </div>
        <div class="col-sm-4">
            <center><img src = "{{ URL('images/image_index2.jpg') }}" height="300" width="300" style = "border-radius: 150px;"></center>            
        </div>
        <div class="col-sm-4">
            <center><img src = "{{ URL('images/image_index3.jpg') }}" height="300" width="300" style = "border-radius: 150px;"></center>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <center><img src = "{{ URL('images/image_index4.jpg') }}" height="300" width="300" style = "border-radius: 150px;"></center>
        </div>
        <div class="col-sm-4">
            <center><img src = "{{ URL('images/image_index5.jpg') }}" height="300" width="300" style = "border-radius: 150px;"></center>            
        </div>
        <div class="col-sm-4">
            <center><img src = "{{ URL('images/image_index6.jpg') }}" height="300" width="300" style = "border-radius: 150px;"></center>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <center><img src = "{{ URL('images/image_index7.jpg') }}" height="300" width="300" style = "border-radius: 150px;"></center>
        </div>
        <div class="col-sm-4">
            <center><img src = "{{ URL('images/image_index8.jpg') }}" height="300" width="300" style = "border-radius: 150px;"></center>            
        </div>
        <div class="col-sm-4">
            <center><img src = "{{ URL('images/image_index9.jpg') }}" height="300" width="300" style = "border-radius: 150px;"></center>
        </div>
    </div>
</div>
<br><br><br>
</body>
<footer class="page-footer font-small" style = "background-color: black; color:white;">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2022 Copyright:
    <a href="/" style = "text-decoration:none; color:white;"> Joshua Maurice Yaacoub</a>
  </div>
  <!-- Copyright -->
</footer>
</html>