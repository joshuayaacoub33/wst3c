<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Title bar icon -->
    <link rel="icon" href="{{ URL('images/favicon.png') }}" type="image/icon type">

    <!-- CSS and JS Libraries -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css" integrity="sha256-mmgLkCYLUQbXn0B1SRqzHar6dCnv9oZFPEC1g1cwlkk=" crossorigin="anonymous" />
    <style>
        header {
            height:40vh;
            min-height:500px;
            background-size:cover;
            background-color: black;
        }

        .hidden-spacer {
            background-color: black;
            height: 56px
        }
        p{
            font-size: 20px;
        }
        .section {
            padding: 100px 0;
            position: relative;
        }
        .gray-bg {
            background-color: #f5f5f5;
        }
        .section-title h2 {
            font-weight: 700;
            color: #20247b;
            font-size: 45px;
            margin: 0 0 15px;
            border-left: 5px solid #fc5356;
            padding-left: 15px;
        }
        .section-title {
            padding-bottom: 45px;
        }
        .section-title p {
            margin: 0;
            font-size: 18px;
        }

        /* Resume Box
        ---------------------*/
        .resume-box {
            background: #ffffff;
            box-shadow: 0 0 1.25rem rgba(31, 45, 61, 0.08);
            border-radius: 10px;
        }
        .resume-box ul {
            margin: 0;
            padding: 30px 20px;
            list-style: none;
        }
        .resume-box li {
            position: relative;
            padding: 0 20px 0 60px;
            margin: 0 0 30px;
        }
        .resume-box li:last-child {
            margin-bottom: 0;
        }
        .resume-box li:after {
            content: "";
            position: absolute;
            top: 0px;
            left: 20px;
            border-left: 1px dashed #fc5356;
            bottom: 0;
        }
        .resume-box .icon {
            width: 40px;
            height: 40px;
            position: absolute;
            left: 0;
            right: 0;
            color: #fc5356;
            line-height: 40px;
            background: #ffffff;
            text-align: center;
            z-index: 1;
            border: 1px dashed;
            border-radius: 50%;
        }
        .resume-box .time {
            background: #fc5356;
            color: #ffffff;
            font-size: 10px;
            padding: 2px 10px;
            display: inline-block;
            margin-bottom: 12px;
            border-radius: 20px;
            font-weight: 600;
        }
        .resume-box h5 {
            font-weight: 700;
            color: #20247b;
            font-size: 16px;
            margin-bottom: 10px;
        }
        .resume-box p {
            margin: 0;
        }

        .resume-box li:after {
            content: "";
            position: absolute;
            top: 0px;
            left: 20px;
            border-left: 1px dashed #fc5356;
            bottom: 0;
        }
    </style>
    <title>About Me</title>
</head>
<body>
<header>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <center>
                        <img src = "{{ URL('images/image_index2.jpg')}}" style = "margin-top: 100px; margin-right: 20px; border-radius: 150px;" height = "300px" width = "300px">   
                        <br>
                        <h1 style = "color:white; margin-top: 20px;">Joshua Maurice Yaacoub</h1>
                    </center> 
                </div>
            </div>
        </div>
</header>
<nav class="navbar navbar-expand-sm fixed-top navbar-dark">
    <div class="container">
        <a class="navbar-brand" href="/">Home</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar1">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar1">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="/about">About me</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/registration">Registration</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/login">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/gallery">Gallery</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="sticky-top bg-black hidden-spacer"></div>
<br><br>
<section class="section gray-bg" id="resume">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2>About Me</h2>
                <p>Hi I'm Joshua Maurice C. Yaacoub, I lived in Nancayasan, Urdaneta City, Pangasinan, I'm 20 years and was born on August 29, 2001
    , I am 3rd year Information Technology Student at Pangasinan State University majoring in web and mobile application development.
    I am always focused more in mobile application development but stil also i can develop web application, but i'm confortable right now with developing in mobile application.
    Java was my first language that i learned a lot, before moving into another programming language. I realized that learning one programming language is the best choice,
    since it is effective, i can easily switch to another programming language, i already the basic syntax and fundamentals of Programming Language together with OOP Concepts,
    but still i need to know more advance coding techniques.
    </p>

    <p>Before being an IT Student, i was before STEM student, solving problems and memorizing formulas, and i worked as trainee in Dagupan, Creotec Inc. It is an electronics company.
        I am functionality tester of the pcb board, we are groups of assembly staff in the laboratory, we assemble the PCB board. Our job was to test if there is defective board
        before going into deployment, to make sure that the board is not defective and fully working.
    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="section-title">
                    <h2>Objectives</h2>
                    <p>To obtain a fresher position as a software engineer in a fast-paced organization where technical skills and creative thinking are useful. 
                    A highly motivated software engineer seeking to get a position in a reputed company, 
                    where I can use my skills and knowledge to learn new things and grow as a software developer.</p>
                </div>
            </div>
        </div>
        <div class="row">
        <div class="col-lg-6">
                <div class="section-title">
                    <h2>Educational Attainment</h2>
                    <p>Student Experience</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="section-title">
                    <h2>Experience</h2>
                    <p>Working Experience</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 m-15px-tb">
                <div class="resume-box">
                    <ul>
                        <li>
                            <div class="icon">
                                <i class="fas fa-user-graduate"></i>
                            </div>
                            <span class="time">2017 - 2019</span>
                            <h5>Our Lady of the Lilies Academy</h5>
                            <p>Graduated Senior High School - STEM Strand</p>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fas fa-user-graduate"></i>
                            </div>
                            <span class="time">2013 - 2017</span>
                            <h5>Our Lady of the Lilies Academy</h5>
                            <p>Graduated Junior High School</p>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fas fa-user-graduate"></i>
                            </div>
                            <span class="time">2007 - 2013</span>
                            <h5>Our Lady of the Lilies Academy</h5>
                            <p>Graduated Elementary</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 m-15px-tb">
                <div class="resume-box">
                    <ul>
                        <li>
                            <div class="icon">
                                <i class="fas fa-briefcase"></i>
                            </div>
                            <span class="time">January 2019 - February 2019</span>
                            <h5>Functionality Tester</h5>
                            <p>To test and implement the functionality of the PCB Board Sensored Devices and check properly if it is working well.</p>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fas fa-briefcase"></i>
                            </div>
                            <span class="time">2021 - Present</span>
                            <h5>Junior Programmer (JAVA)</h5>
                            <p>Responsible for the design, development, and management of Java-based applications</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2>Certificates</h2>
                    <div class="row">
                        <div class="column" style = "margin: 20px;">
                            <img src = "{{ URL('images/java.jpg') }}" height = "350" width="600">
                            <img src = "{{ URL('images/OOP.jpg') }}" height = "350" width="600">
                            <img src = "{{ URL('images/sql.jpg') }}" height = "350" width="600">
                            <img src = "{{ URL('images/javaHack.png') }}" height = "350" width="600">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</body>
<footer class="page-footer font-small" style = "background-color: black; color:white;">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2022 Copyright:
    <a href="/" style = "text-decoration:none; color:white;"> Joshua Maurice Yaacoub</a>
  </div>
  <!-- Copyright -->
</footer>
</html>