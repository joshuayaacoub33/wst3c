<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel - Activity 1 Route</title>

        <!-- CSS and JS Libraries from Bootstrap 5 -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
            .float{
                position: relative;
                left: 150px;
                top: 20px;
                z-index: 2;
                background-color: white;
            }
        </style>
    </head>
    <body>
        <label class = "float"><b>Personal Information</b></label>
        <div class="container">
            <div class="row">
                <div class="col-mx-auto mt-2">
                    <div class="card mb-4">
                    <div class="card-body mt-4">
                        <div class="col-sm-4 m-2">
                            <label>Firstname:</label>
                            <input type = "text" class = "form-control" placeholder = "Enter your firstname" name = "fname">
                        </div>
                        <div class="col-sm-4 m-2">
                            <label>Lastname:</label>
                            <input type = "text" class = "form-control" placeholder = "Enter your lastname" name = "lname">
                        </div>
                        <div class="col-sm-4 m-2">
                            <label>Username:</label>
                            <input type = "text" class = "form-control" placeholder = "Enter your username" name = "uname">
                        </div>
                        <div class="col-sm-4 m-2">
                            <label>Password:</label>
                            <input type = "password" class = "form-control" placeholder = "Enter your password" name = "pass"
                            style = "border: #FF0000 solid 7px;">
                        </div>
                        <br>
                        <div class="col-sm-6 m-2">
                            <textarea class = "form-control" style = "overflow-y: scroll; padding-bottom: 50px;"></textarea>
                        </div>
                        <br>
                        <div class="col-sm-4 m-2">
                            <label>Birthdate:</label>
                            <input type = "date" class = "form-select" placeholder="dd-mm-yyyy">
                            <label style = "color:#FF0000;">Town: </label>
                            <select name = "town" class = "form-select">
                                <option value = "Nancayasan">Nancayasan</option>
                                <option value = "Palina East">Palina East</option>
                                <option value = "Poblacion">Poblacion</option>
                                <option value = "Oltama">Oltama</option>
                                <option value = "Sugcong">Sugcong</option>
                                <option value = "Oltama">Labit Proper</option>
                                <option value = "Sugcong">Catablan</option>
                            </select>
                            <label>Browser: </label>
                            <input type = "text" class = "form-control"/>
                            <br>
                        </div>
                        <div class="col-sm-4 m-2">
                            <div class="btn-group">
                                <button class = "btn btn-success" style = "border: #FF0000 solid 3px; border-radius:1px;">Click me</button>&nbsp;&nbsp
                                <button style = "border: solid 1px; border-radius:5px;">Reset</button>&nbsp;&nbsp;
                                <button style = "border: solid 1px; border-radius:5px;">Pindot Me</button>
                            </div>
                        </div>
                        <br>
                    </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </body>
</html>