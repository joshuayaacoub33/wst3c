<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/admin/dashboard', [AdminController::class, 'index']);
Route::get('/admin/dashboard/approved/{id}', [AdminController::class, 'updateApproved']);
Route::get('/admin/dashboard/disapproved/{id}', [AdminController::class, 'updateReject']);

Route::get('/find_appointment/{id?}', [AppointmentController::class, 'findAppointments']);
Route::get('/approved_appointment/{id?}', [AppointmentController::class, 'approvedAppointments']);
Route::get('/dashboard', [AppointmentController::class, 'restrictAppointment']);

Route::get('/', [LoginController::class, 'index']);
Route::get('/dashboard', [LoginController::class, 'successLogin']);
Route::post('/validate', [LoginController::class, 'validateForm'])->name('login.check');
Route::get('/logout', [LoginController::class, 'logout']);

Route::get('/register', [RegisterController::class, 'index']);
Route::post('/register/create', [RegisterController::class, 'validateForm'])->name('create.account');

Route::get('/dashboard', [AppointmentController::class, 'index']);
Route::post('/dashboard', [AppointmentController::class, 'validateForm'])->name('create.appointment');