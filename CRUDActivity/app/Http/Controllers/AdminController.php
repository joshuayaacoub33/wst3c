<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function index()
    {
        $join = DB::select(
            'select * from appointments 
            INNER JOIN appointment_times ON appointments.time_id = appointment_times.time_id
            INNER JOIN users ON appointments.id = users.id'
        );
        return view('admin.dashboard')->with('appointment', $join);
    }

    public function updateApproved($id)
    {
        DB::update('update appointments set status = ? where appointment_id = ?', ['APPROVED', $id]);
        $approved = DB::insert('insert into appointment_histories(appointment_id, id, time_id, booked_date, message, status) 
        select * from appointments where appointment_id = ?', [$id]);
        DB::delete('delete from appointments where appointment_id = ?', [$id]);
        return redirect('/admin/dashboard')->with('appointment', $approved);
    }

    public function updateReject($id)
    {
        DB::update('update appointments set status = ? where appointment_id = ?', ['DISAPPROVED', $id]);
        $approved = DB::insert('insert into appointment_histories(appointment_id, id, time_id, booked_date, message, status) 
        select * from appointments where appointment_id = ?', [$id]);
        DB::delete('delete from appointments where appointment_id = ?', [$id]);
        return redirect('/admin/dashboard')->with('appointment', $approved);
    } 
}
