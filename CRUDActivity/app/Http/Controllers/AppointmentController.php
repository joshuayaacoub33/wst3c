<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AppointmentController extends Controller
{
    public function index()
    {
        $book = DB::select('select * from appointment_times');
        return view('dashboard')->with('appoint', $book);
    }

    public function findAppointments($id = null){
        $appointment = DB::select(
            'select * from appointments
            INNER JOIN appointment_times ON appointments.time_id = appointment_times.time_id
            INNER JOIN users ON appointments.id = users.id
            where appointments.id = ?
        ', [$id]
        );
        return view('find_appointment')->with('appointment', $appointment);
    }

    public function approvedAppointments($id = null){
        $appointment = DB::select(
            'select * from appointment_histories
            INNER JOIN appointment_times ON appointment_histories.time_id = appointment_times.time_id
            INNER JOIN users ON appointment_histories.id = users.id
            where appointment_histories.status = ? AND appointment_histories.id = ?', 
            ['APPROVED', $id]
        );
        return view('approved_appointment')->with('appointment', $appointment);
    }


    public function validateForm(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'timeBook' => 'required', 
            'message' => 'required|max:255',
        ]);
        
        if($validator->fails()){
            return redirect('/dashboard')->withErrors($validator)->withInput();
        }
        else{
            $date = $request->get('date');
            $message = $request->get('message');
            $time_id = $request->get('timeBook');
            $user_id = $request->get('user_id');
            
            $select = DB::select('select * from appointments where time_id = ? AND booked_date = ?', [$time_id, $date]);
            if(count($select) == 0){
                DB::insert('insert into appointments(id, time_id, booked_date, message, status)
                values(?, ?, ?, ?, ?)',[$user_id, $time_id, $date, $message, 'PENDING']);
                return redirect('/dashboard')->with('success', 'Appointment Set Successfully!');
            }
            else{
                return back()->with('error', 'This appointment date and time is already taken, please select another one!');
            }
        }    
    }
}
