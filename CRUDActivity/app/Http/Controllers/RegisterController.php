<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    public function index()
    {
        return view('register');
    }

    public function validateForm(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => ['required'],
            'last_name' => ['required'],
            'address' => ['required'],
            'username' => ['required', 'min:6'],
            'password' => ['required', 'min:6'],
            'gender' => ['required'],
            'birthdate' => ['required'],
            'eMail' => ['required', 'unique:users', 'email'],
        ]);
        
        if($validator->fails()){
            return redirect('/register')->withErrors($validator)->withInput();
        }
        else{
            $fname = $request->get('first_name');
            $lname = $request->get('last_name');
            $address = $request->get('address');
            $username = $request->get('username');
            $password = Hash::make($request->get('password'));
            $gender = $request->get('gender');
            $birthdate = $request->get('birthdate');
            $email = $request->get('eMail');

            DB::insert('insert into users(First_name, Last_name, Address, username, password, Gender, Birthdate, Email)
            values(?,?,?,?,?,?,?,?)', [$fname, $lname, $address, $username, $password, $gender, $birthdate, $email]);

            return redirect('/register')->with('success', 'Registered Successfully!');
        }    
    }
}
