<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function validateForm(Request $request)
    {
        $this->validate($request, [
            'username'=>'required',
            'password'=>'required|alphaNum|min:6',
        ]);

        $user_data = array(
            'username' =>  $request->get('username'),
            'password'  =>  $request->get('password'),
        );

        $username = $request->input('username');
        $password = $request->input('password');

        if(Auth::attempt($user_data)){
            return redirect('/dashboard');
        }
        else if($username == 'admin' && $password == 'admin12'){
            return redirect('/admin/dashboard');
        }
        else{
            return back()->with('error', 'Wrong Login Details!');
        }
        
    }

    public function successLogin()
    {
        return view('dashboard');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
