@if (!isset(Auth::user()->username))
    <script>window.location = "/"</script>
@endif
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ asset('assets/icon/clinic_icon.png') }}" type='image/x-icon'>
    <link href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">​
    <script type = "text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.2.8/css/rowReorder.bootstrap5.min.css"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>​
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    <title>Clinic Appointment System - Find Appointment</title>
    <style>
        body{
            margin-top: -25px;
        }
        table td {
            transition: all .5s;
            max-width: 200px;
            height: 100px;
            white-space:pre-wrap;
            text-overflow: ellipsis;
            word-break:keep-all;
        }
        @import url(//fonts.googleapis.com/css?family=Montserrat:400,500,700);
        body{
          background-image:url(https://imageio.forbes.com/specials-images/imageserve/5dbb4182d85e3000078fddae/0x0.jpg?format=jpg&width=1200); 
          background-size: cover;
        }
      .banner3 {
        font-family: "Montserrat", sans-serif;
        color: #8d97ad;
        font-weight: 300;
        max-height: 800px;
      }

      .banner3 .banner {
        position: relative;
        max-height: 700px;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center top;
        width: 100%;
        display: table;
      }
      

      .banner3 h1,
      .banner3 h2,
      .banner3 h3,
      .banner3 h4,
      .banner3 h5,
      .banner3 h6 {
        color: #3e4555;
      }

      .banner3 .font-weight-medium {
        font-weight: 500;
      }

      .banner3 .subtitle {
        color: #8d97ad;
        line-height: 24px;
      }

      .banner3 .btn-danger-gradiant {
        background: #ff4d7e;
        background: -webkit-linear-gradient(legacy-direction(to right), #ff4d7e 0%, #ff6a5b 100%);
        background: -webkit-gradient(linear, left top, right top, from(#ff4d7e), to(#ff6a5b));
        background: -webkit-linear-gradient(left, #ff4d7e 0%, #ff6a5b 100%);
        background: -o-linear-gradient(left, #ff4d7e 0%, #ff6a5b 100%);
        background: linear-gradient(to right, #ff4d7e 0%, #ff6a5b 100%);
        border: 0px;
      }

      .banner3 .btn-danger-gradiant:hover {
        background: #ff6a5b;
        background: -webkit-linear-gradient(legacy-direction(to right), #ff6a5b 0%, #ff4d7e 100%);
        background: -webkit-gradient(linear, left top, right top, from(#ff6a5b), to(#ff4d7e));
        background: -webkit-linear-gradient(left, #ff6a5b 0%, #ff4d7e 100%);
        background: -o-linear-gradient(left, #ff6a5b 0%, #ff4d7e 100%);
        background: linear-gradient(to right, #ff6a5b 0%, #ff4d7e 100%);
      }

      .banner3 .btn-danger-gradiant.active,
      .banner3 .btn-danger-gradiant:active,
      .banner3 .btn-danger-gradiant:focus {
        -webkit-box-shadow: 0px;
        box-shadow: 0px;
        opacity: 1;
      }


      .banner3 .btn-md {
        padding: 15px 45px;
        font-size: 16px;
      }

      .banner3 .form-row {
        margin: 0;
      }

      .banner3 label.font-12 {
        font-size: 12px;
        font-weight: 500;
        margin-bottom: 5px;
      }

      .banner3 .form-control {
        color: #8d97ad;
        -o-text-overflow: ellipsis;
        text-overflow: ellipsis;
      }

      .banner3 .date label {
        cursor: pointer;
        margin: 0;
      }

      @media (max-width: 370px) {
        .banner3 .left,
        .banner3 .right {
          padding: 25px;
        }
      }

      @media (max-width: 320px) {
        .banner3 .left,
        .banner3 .right {
          padding: 25px 15px;
        }
      }

      .banner3 .font-14 {
        font-size: 14px;
      }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <div class="container-fluid">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="/dashboard">Appointment</a>
        </li>
        @if (isset(Auth::user()->id))
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="/find_appointment/{{ Auth::user()->id }}">Find Appointment</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="/approved_appointment/{{ Auth::user()->id }}">Approved Appointment</a>
        </li>
        @endif
        <li class="nav-item dropdown">
        @if (isset(Auth::user()->username))
        <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Welcome, {{ Auth::user()->username }}
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
            <li><a class="dropdown-item" href="/logout">Logout</a></li> 
        </ul>
        @endif
        </li>
      </ul>
    </div>
  </div>
</nav>

<div class="banner3" style="margin-bottom: 100px;">
  <div class="py-5 banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <center><h3 class="my-3 text-white font-weight-medium text-uppercase">Find Appointment</h3></center>
          @if (isset(Auth::user()->id))
          <input type="hidden" name="user_id" value = "{{ Auth::user()->id }}">
          @endif
          <div class="bg-white" style="border-radius: 10px;">
          <div class="container-fluid overflow-hidden mx-auto p-5">
                <div class="row overflow-hidden">
                    <div class="col-sm-12 mx-auto overflow-hidden">
                    <table id="example" class="table table-striped table-bordered dt-responsive" style="width:100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Booked Time</th>
                                <th>Booked Date</th>
                                <th>Message</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($appointment as $app)
                            <tr>
                                <td>{{ $app->First_name }} {{ $app->Last_name }}</td>
                                <td>{{ $app->book_time }}</td>
                                <td>{{ $app->booked_date }}</td>
                                <td>{{ $app->message }}</td>
                                <td><center><b>{{ $app->status }}</b></center></td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>      
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="py-3 px-4 px-xl-5 bg-primary fixed-bottom">
    <!-- Copyright -->
    <div class="text-white mb-2 mb-md-0">
      Copyright © 2022. All rights reserved.
    </div>
  </div>
</body>
</html>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            select: true,
            scrollX: true,
            class: "custom-class",
        });
    });
</script>