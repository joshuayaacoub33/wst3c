@if (isset(Auth::user()->Username))
    <script>window.location = "/"</script>
@endif
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ asset('assets/icon/clinic_icon.png') }}" type='image/x-icon'>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <title>Clinic Appointment System</title>
    <style>
        .divider:after,
        .divider:before {
        content: "";
        flex: 1;
        height: 1px;
        background: #eee;
        }
        .h-custom {
        height: calc(100% - 73px);
        }
        @media (max-width: 450px) {
        .h-custom {
        height: 100%;
        }
        }
        body {
  display: flex;
  min-height: 100vh;
  flex-direction: column;
}
    </style>
</head>
<body>
<section class="vh-100">
  <div class="container-fluid h-custom">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-md-8 col-lg-5 col-xl-5">
        <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.webp"
          class="img-fluid" alt="Sample image">
      </div>
      <div class="col-md-8 col-lg-6 col-xl-5 offset-xl-1">
        <form id = "register" method="POST" action = "{{ route('create.account') }}">
          {{ csrf_field() }}
          <div class="divider d-flex align-items-center my-4">
            <p class="text-center fw-bold mx-3 mb-0"><h1>Sign Up</h1></p>
          </div>
          @if (count($errors) > 0)
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif
          @if(session()->has('success'))
              <div class="alert alert-success">
                  {{ session()->get('success') }}
              </div>
          @endif
          <div class="row">
              <div class="col-md-6">
                <div class="form-outline mb-4">
                    <label class="form-label" for="form3Example1m">First name:</label>
                    <input type="text" id="form3Example1m" class="form-control" name = "first_name" placeholder="First Name"/>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-outline mb-4">
                    <label class="form-label" for="form3Example1n">Last name:</label>
                    <input type="text" id="form3Example1n" class="form-control" name = "last_name" placeholder="Last Name"/>
                </div>
              </div>
          </div>
          <div class="col-md-12">
                <div class="form-outline mb-4">
                    <label class="form-label" for="form3Example8">Address:</label>
                    <input type="text" id="form3Example8" class="form-control" name = "address" placeholder="Address"/>
                </div>
          </div>
          <div class="row">
              <div class="col-md-6">
                <div class="form-outline mb-4">
                    <label class="form-label" for="form3Example1m1">Username:</label>
                    <input type="text" id="form3Example1m1" class="form-control" name = "username" placeholder="Username"/>
                </div>
              </div>
              <div class="col-md-6">
                    <div class="form-outline mb-4">
                        <label class="form-label" for="form3Example1n1">Password:</label>
                        <input type="password" id="form3Example1n1" class="form-control" name = "password" placeholder="Password"/>
                    </div>
              </div>
          </div>
            <div class="row">
                <div class="col-md-6">
                    <label class = "form-label">Gender: </label>
                    <div class="form-outline mb-4">
                        <select class = "form-select" name = "gender">
                            <option value = "male">Male</option>
                            <option value = "female">Female</option>
                            <option value = "other">Other</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-outline mb-4"> 
                        <label class="form-label" for="form3Example9">Birthdate:</label>
                        <input type="date" id="form3Example9" class="form-control" name = "birthdate"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-outline mb-2">
                        <label class="form-label" for="form3Example97">Email:</label>
                        <input type="email" id="form3Example97" class="form-control" name = "eMail" placeholder="Email"/>
                    </div>
                </div>
            </div>

          <div class="text-center text-lg-start mt-1 pt-2" style="margin-bottom: 100px;">
            <button type="submit" class="btn btn-primary btn-lg"
              style="padding-left: 2.5rem; padding-right: 2.5rem;">Register</button>
            <p class="small fw-bold mt-2 pt-1">Already have an account? <a href="/"
                class="link-danger">Login</a></p>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="py-3 px-4 px-xl-5 bg-primary fixed-bottom">
    <!-- Copyright -->
    <div class="text-white mb-2 mb-md-0">
      Copyright © 2022. All rights reserved.
    </div>
  </div>
</section>
</body>
</html>
<script>
    function clearFields()
    {
        document.getElementById("register").reset();
    }
</script>