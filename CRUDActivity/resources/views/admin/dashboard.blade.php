<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ asset('assets/icon/clinic_icon.png') }}" type='image/x-icon'>
    <link href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">​
    <script type = "text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.2.8/css/rowReorder.bootstrap5.min.css"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>​
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    <title>Clinic Appointment System - Approved Appointment</title>
    <style>
        
        body{
            margin-top: -25px;
            background-repeat: no-repeat;
            background-size: contain;
        }
        table td {
            transition: all .5s;
            max-width: 200px;
            height: 100px;
            white-space: pre-wrap;
            text-overflow: ellipsis;
            word-break: break-all;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <div class="container-fluid">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="/admin/dashboard">Appointment</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="/logout">Logout</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div class="container mt-5">
    <h1>Approval of Appointments</h1>
</div>

<div class="container-fluid overflow-hidden mx-auto p-5" style="margin-bottom: 100px;">
    <div class="row overflow-hidden">
        <div class="col-sm-10 mx-auto overflow-hidden">
        <table id="example" class="table table-striped table-bordered dt-responsive" style="width:100%">
            <thead>
                <tr>
                    <th>Appointment ID</th>
                    <th>Name</th>
                    <th>Booked Time</th>
                    <th>Booked Date</th>
                    <th>Message</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($appointment as $app)
                <tr>
                    <td>{{ $app->appointment_id }}</td>
                    <td>{{ $app->First_name }} {{ $app->Last_name }}</td>
                    <td>{{ $app->book_time }}</td>
                    <td>{{ $app->booked_date }}</td>
                    <td>{{ $app->message }}</td>
                    <td>
                        @if ($app->status == "APPROVED")
                            <center><b>{{ $app->status }}</b></center>
                        @elseif($app->status == "DISAPPROVED")
                            <center><b>{{ $app->status }}</b></center>
                        @else
                            <div class="btn-toolbar">
                               <center>
                               <a href="/admin/dashboard/approved/{{ $app->appointment_id }}"><button type="button" class = "btn btn-success">APPROVED</button></a>
                                <a href="/admin/dashboard/disapproved/{{ $app->appointment_id }}"><button type="button" class = "btn btn-danger">DISAPPROVED</button></a>
                               </center>
                            </div>
                        @endif
                    </td>
                </tr>
                @endforeach
                
            </tbody>
        </table>
        </div>
    </div>
</div>
<div class="py-3 px-4 px-xl-5 bg-primary fixed-bottom">
    <!-- Copyright -->
    <div class="text-white mb-2 mb-md-0">
      Copyright © 2022. All rights reserved.
    </div>
  </div>
</body>
</html>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            select: true,
            scrollX: true,
            class: "custom-class",
        });
    });
</script>