@if(isset(Auth::user()->username))
    <script>window.location = "/admin/dashboard"</script>
@endif
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="{{ url('../css/style.css') }}">
    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script src="https://kit.fontawesome.com/6d6b82be0b.js" crossorigin="anonymous"></script>
    <script type='text/javascript' src=''></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Caveat:wght@500&display=swap');
        .footer-text-style {
            font-family: 'Caveat', cursive;
        }
    </style>
</head>
<body>
    <div class="dflex justify-content-between bg bg-primary text-light p-3 align-items-center shadow nav">
        <div>
            <h2 style="margin: 0">Login</h2>
        </div>
    </div>
    <div class="container mt-5">
        <center>
            <div class="col-sm-6">
                <form id = "login" action="{{ route('check.login') }}" method="post">
                    {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header">
                            <h3>ADMIN LOGIN</h3>
                        </div>
                        <div class="card-body">
                            @if (Session::has('error'))
                                <div class="alert alert-danger m-3">
                                    {{ Session::get('error') }}
                                </div>
                            @endif
                            <div class="col-sm-12">
                                <label>Username:</label>
                                <input type = "text" name = "username" id = "username" class = "form-control" placeholder="Username">
                                @error('username')
                                    <div class = "text text-danger d-flex">{{ $message }}</div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-sm-12">
                                <label>Password:</label>
                                <input type = "password" name = "password" id = "password" class = "form-control" placeholder="Password">
                                @error('password')
                                    <div class="text text-danger d-flex">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Login</button>
                            <button type="button" class="btn btn-secondary" onclick="resetForm()">Reset</button>
                            <a href = "/register"><button type="button" class="btn btn-danger">Register</button></a>
                        </div>
                    </div>
                </form>
            </div>
        </center>
    </div>
</body>
<footer class="page-footer font-small justify-content-between p-3 text-light footer-text-style bg bg-primary align-items-center shadow nav fixed-bottom">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3"><h4 style="margin: 0">©{{ Date("Y") }} Copyright: PSU</h4>
    <a href="/" class="text-white"> </a>
  </div>
  <!-- Copyright -->

</footer>
</html>
<script>
    function resetForm()
    {
        $('#login')[0].reset();
    }
</script>