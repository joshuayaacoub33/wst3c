<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ url('../css/style.css') }}">
    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    <script src="https://kit.fontawesome.com/6d6b82be0b.js" crossorigin="anonymous"></script>
    <script type='text/javascript' src=''></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <title>Register</title>
</head>
<body>
<div class="container mt-5">
        <center>
            <div class="col-sm-6">
                <form id = "register" action="{{ route('create.user') }}" method="post">
                    {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header">
                            <h3>REGISTER</h3>
                        </div>
                        <div class="card-body">
                            @if (Session::has('success'))
                                <div class="alert alert-success m-3">
                                    {{ Session::get('success') }}
                                </div>
                            @endif
                            <div class="col-sm-12">
                                <label>Username:</label>
                                <input type = "text" name = "username" id = "username" class = "form-control" placeholder="Username">
                                @error('username')
                                    <div class = "text text-danger d-flex">{{ $message }}</div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-sm-12">
                                <label>Email:</label>
                                <input type = "text" name = "email" id = "email" class = "form-control" placeholder="Email Address">
                                @error('email')
                                    <div class = "text text-danger d-flex">{{ $message }}</div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-sm-12">
                                <label>Password:</label>
                                <input type = "password" name = "password" id = "password" class = "form-control" placeholder="Password">
                                @error('password')
                                    <div class="text text-danger d-flex">{{ $message }}</div>
                                @enderror
                            </div>
                            <br>
                            <div class="col-sm-12">
                                <label>Password:</label>
                                <input type = "password" name = "confirm_password" id = "confirm_password" class = "form-control" placeholder="Confirm Password">
                                @error('confirm_password')
                                    <div class="text text-danger d-flex">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Register</button>
                            <button type="button" class="btn btn-secondary" onclick="resetForm()">Reset</button> 
                            <a href = "/login"><button type="button" class="btn btn-danger">Login</button></a>
                        </div>
                    </div>
                </form>
            </div>
        </center>
    </div>
</body>
</html>
<script>
    function resetForm()
    {
        $('#register')[0].reset();
    }
</script>