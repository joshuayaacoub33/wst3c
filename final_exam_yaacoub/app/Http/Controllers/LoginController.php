<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function index()
    {
        return view('login');
    }
    
    public function validateLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required|min:6',
        ]);

        $user_data = array(
            'username' => $request->get('username'),
            'password' => $request->get('password')
        );

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else{
            if(Auth::attempt($user_data)){
                return redirect('/dashboard');
            }else{
                return redirect()->back()->with('error', 'Invalid username or password');
            }
        }
    }

    public function success()
    {
        return view('users.dashboard');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
