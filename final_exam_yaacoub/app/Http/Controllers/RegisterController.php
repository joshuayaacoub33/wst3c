<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    public function index()
    {
        return view('register');
    }

    public function validateRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|unique:users|min:6',
            'email' => 'required|unique:users',
            'password' => 'required_with:confirm_password|same:confirm_password|min:6',
            'confirm_password' => 'required|min:6',
        ]);

        $user = new User();
        $username = $request->get('username');
        $email = $request->get('email');
        $password = $request->get('password');

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else{
            $user->username = $username;
            $user->email = $email;
            $user->password = Hash::make($password);
            $user->userType = 'user';
            $user->save();
            return redirect()->back()->with('success', 'Registered Succesfully!');
        }
    }
}
