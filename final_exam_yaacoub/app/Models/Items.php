<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    use HasFactory;

    public $primary_key = 'id';
    public $timestamps = true;

    protected $fillable = [
        'name',
        'price',
        'stocks',
        'date_created'
    ];
}
