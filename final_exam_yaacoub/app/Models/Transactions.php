<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    use HasFactory;

    public $primary_key = 'id';
    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'product_id',
        'quantity',
    ];
}
