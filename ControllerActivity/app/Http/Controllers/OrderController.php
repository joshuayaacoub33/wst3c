<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function customer($cusID, $name, $addr)
    {
        return view('customer', ['cusID' => $cusID, 'name' => $name, 'addr' => $addr]);
    }

    public function item($itemNo, $name, $price)
    {
        return view('item', ['itemNo' => $itemNo, 'name' => $name, 'price' => $price]);
    }

    public function order($cusID, $name, $orderNo, $date)
    {
        return view('order', ['cusID' => $cusID, 'name' => $name, 'orderNo' => $orderNo, 'date' => $date]);
    }

    public function orderDetails($transNo, $orderNo, $itemID, $name, $price, $qty)
    {
        return view('order_details', ['transNo' => $transNo, 'orderNo' => $orderNo, 'itemID' => $itemID, 'name' => $name, 'price' => $price, 'qty' => $qty]);
    }
}
