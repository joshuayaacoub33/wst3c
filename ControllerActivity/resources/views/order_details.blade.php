<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <title>Order Details</title>
    <style>
        a{
            text-decoration: none;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row m-5">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3>Order Details</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Transaction No:</label>
                                <input type = "text" class = "form-control" value = "<?=$transNo?>" readonly><br>
                                <label>Order No</label>
                                <input type = "text" class = "form-control" value = "<?=$orderNo?>" readonly><br>
                                <label>Item No:</label>
                                <input type = "text" class = "form-control" value = "<?=$itemID?>" readonly><br>
                                <label>Name</label>
                                <input type = "text" class = "form-control" value = "<?=$name?>" readonly><br>
                                <label>Price:</label>
                                <input type = "text" class = "form-control" value = "<?=$price?>" readonly><br>
                                <label>Qty:</label>
                                <input type = "text" class = "form-control" value = "<?=$qty?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href = "/">Back</a>
                    </div> 
                </div>
            </div>
        </div>
    </div> 
</body>
</html>