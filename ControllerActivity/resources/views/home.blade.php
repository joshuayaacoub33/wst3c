<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <title>Homepage</title>
    <style>
        a{
            text-decoration: none;
            color: black;
            font-weight: bold;
        }

        a:hover{
            color:white;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row m-5">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3>Welcome to Restaurant</h3>
                    </div>
                    <div class="card-body bg-dark text-light">
                        <div class="card bg-warning  p-2">
                            <a href = "{{ route('customer', ['cusID' => 1, 'name' => 'Joshua', 'addr' => 'Urdaneta']) }}">Customer</a><br>
                        </div>
                        <div class="card bg-primary p-2 mt-3">
                            <a href = "{{ route('item', ['itemNo' => 'i-111', 'name' => 'Biryani', 'price' => 25]) }}">Item</a><br>
                        </div>
                        <div class="card bg-danger p-2 mt-3">
                            <a href = "{{ route('order', ['cusID' => 1, 'name' => 'Biryani', 'orderNo' => 'o-1234', 'date' => date('Y-m-d')]) }}">Order</a><br>
                        </div>
                        <div class="card bg-secondary p-2 mt-3">
                            <a href = "{{ route('orderDetails', ['transNo' => 't-111', 'orderNo' => 'o-1234', 'itemID' => 'i-111', 'name' => 'Biryani', 'price' => 125, 'qty' => 2])}}">Order Details</a>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</body>
</html>