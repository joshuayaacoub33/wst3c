<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/Customer/{customerID}/{Name}/{Age?}', function ($customerID=111, $Name="Joshua", $Age="20") {
    return view('customer')->with('id', $customerID)->with('name', $Name)->with('age', $Age);
});


Route::get('Customer/item',function($item_no=1, $name="Biryani", $price="Php 125")  
{  
    $url=route('Customer.item');  
    return view('item')->with('item_no', $item_no)->with('item_name', $name)->with('item_price', $price);  
})->name('Customer.item'); 

Route::get('item/order', function($customerID=111, $name="Joshua", $orderNo="12345", $date="11-11-2022")  
{  
    $url=route('item.order');  
    return view('order')->with('cusID', $customerID)->with('name', $name)->with('orderNo', $orderNo)
    ->with('date', $date);  
})->name('item.order');

Route::get('order/orderDetails', function($transNo=10010, $orderNo = "12345", $item_no=1, $name="Joshua", $price = "Php 125", $qty = 2)  
{  
    $url=route('order.orderDetails');  
    return view('orderDetails')->with('trNo', $transNo)->with('itemNo', $item_no)->with('orderNo', $orderNo)
    ->with('name', $name)->with('price', $price)->with('qty', $qty);  
})->name('order.orderDetails');
